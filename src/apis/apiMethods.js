import axios from "axios";

export const baseUrl = process.env.REACT_APP_BASE_URL;

const headers = () => {
  const token = localStorage.getItem("userToken");
  if (!token) {
    return null;
  }
  return {
    Authorization: `Bearer ${token}`,
  };
};

const axiosInstance = axios.create({
  baseURL: baseUrl,
  headers: headers(),
});

export const postData = (url, data) => {
  return axiosInstance.post(url, data, {
    headers: headers(),
  });
};

export const getData = (url) => {
  return axiosInstance.get(url, {
    headers: headers(),
  });
};

export const patchData = (url, data) => {
  return axiosInstance.patch(url, data, {
    headers: headers(),
  });
};

export const putData = (url, data) => {
  return axiosInstance.put(url, data, {
    headers: headers(),
  });
};

export const deleteData = (url) => {
  return axiosInstance.delete(url);
};
