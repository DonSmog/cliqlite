import React, { useEffect, useRef, useState } from "react";
import { RiMore2Fill } from "react-icons/ri";
import { useNavigate } from "react-router-dom";
import ROUTES from "../Helpers/routes";

const QuizItem = ({ photo, title, option1, option2, option3 }) => {
  const navigate = useNavigate();

  const [dropdown, setDropdown] = useState(false);
  const ref = useRef();

  useEffect(() => {
    const handleClick = (e) => {
      if (dropdown && ref.current && !ref.current.contains(e.target)) {
        setDropdown(false);
      }
    };

    document.addEventListener("click", handleClick);

    return () => {
      document.removeEventListener("click", handleClick);
    };
  }, [dropdown]);

  const handleDropdown = () => {
    setDropdown(!dropdown);
  };

  const handleAddQuestions = () => {
    navigate(ROUTES.ADD_QUESTIONS);
  };

  return (
    <div className="video-item">
      <div className="video-item-image">
        <img src={photo} alt="Title" />
      </div>
      <div className="video-item-body">
        <div className="video-item-title">
          {title}
          <div className="video-item-icon" onClick={handleDropdown}>
            <RiMore2Fill />
            {dropdown ? (
              <div className="video-item-dropdown" ref={ref}>
                <p>{option1}</p>
                <p onClick={handleAddQuestions}>{option2}</p>
                <p>{option3}</p>
              </div>
            ) : null}
          </div>
        </div>
        <p>3min 40sec</p>
      </div>
    </div>
  );
};

export default QuizItem;
