import React from "react";
import "../styles/button.css";

const Button = ({ children, handleSubmit, htmlFor, disabled }) => {
  return (
    <button
      disabled={disabled}
      htmlFor={htmlFor}
      type="submit"
      className="button"
      onClick={handleSubmit}
    >
      {children}
    </button>
  );
};

export default Button;
