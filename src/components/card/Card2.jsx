import React from "react";
import "./card2.css";

const Card2 = ({ children, title, text, icon }) => {
  return (
    <div className="card2">
      <h4>{title}</h4>
      <p>{text}</p>
      {children}
    </div>
  );
};

export default Card2;
