/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect, useRef } from "react";
import "../styles/video-item.css";

import { RiMore2Fill } from "react-icons/ri";

const VideoItem = ({ option1, option2, name, duration, url }) => {
  const [dropdown, setDropdown] = useState(false);

  const ref = useRef();

  useEffect(() => {
    const handleClick = (e) => {
      if (dropdown && ref.current && !ref.current.contains(e.target)) {
        setDropdown(false);
      }
    };

    document.addEventListener("click", handleClick);

    return () => {
      document.removeEventListener("click", handleClick);
    };
  }, [dropdown]);

  const handleDropdown = () => {
    setDropdown(!dropdown);
  };

  return (
    <div className="video-item">
      <div className="video-item-image">
        <video type="video/mp4" src={url} controls controlsList="nodownload" />
      </div>
      <div className="video-item-body">
        <div className="video-item-title">
          {name}
          <div className="video-item-icon" onClick={handleDropdown}>
            <RiMore2Fill />
            {dropdown ? (
              <div className="video-item-dropdown" ref={ref}>
                <p>{option1}</p>
                <p>{option2}</p>
              </div>
            ) : null}
          </div>
        </div>
        <p>{duration}</p>
      </div>
    </div>
  );
};

export default VideoItem;
