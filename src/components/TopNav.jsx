import React, { useEffect, useRef, useState } from "react";
import "../styles/topnav.css";
import { BsBell } from "react-icons/bs";
import { FaAngleDown, FaSearch } from "react-icons/fa";
import { Link, useNavigate } from "react-router-dom";
import { useAuth } from "../context/auth.context";
import ROUTES from "../Helpers/routes";

const imageUrl =
  "https://www.uni-giessen.de/international-pages/vip/support/ementors/male/image_preview";

const TopNav = () => {
  const [searchTerm, setSearchTerm] = useState("");
  const [dropdown, setDropdown] = useState(false);
  const ref = useRef();
  const { profilePicture } = useAuth();

  useEffect(() => {
    const handleClick = (e) => {
      if (dropdown && ref.current && !ref.current.contains(e.target)) {
        setDropdown(false);
      }
    };

    document.addEventListener("click", handleClick);

    return () => {
      document.removeEventListener("click", handleClick);
    };
  }, [dropdown]);

  const handleOnChange = (e) => {
    setSearchTerm(e.target.value);
  };

  return (
    <div className="topnav">
      <form className="topnav_search">
        <div className="icon">
          <FaSearch />
        </div>
        <input
          onChange={handleOnChange}
          type="text"
          value={searchTerm}
          placeholder="Search"
        />
      </form>
      <div className="topnav_right">
        <BsBell />

        <div className="topnav_user">
          <div className="topnav_user_image">
            {profilePicture ? (
              <img
                src={URL.createObjectURL(profilePicture)}
                alt="User Profile"
              />
            ) : (
              <img src={imageUrl} alt="User Profile" />
            )}
          </div>
          <div className="topnav_user_name">
            <h5>Tutor</h5>
          </div>
        </div>

        <div className="dropdown-button">
          <div ref={ref} onClick={() => setDropdown(!dropdown)}>
            {<FaAngleDown />}
          </div>
          {dropdown ? <TopNavDropdown /> : null}
        </div>
      </div>
    </div>
  );
};

const TopNavDropdown = () => {
  const { logOut } = useAuth();
  const navigate = useNavigate();

  const handleLogOut = () => {
    logOut();
    navigate(ROUTES.LOGIN);
  };
  return (
    <div className="topnav-dropdown">
      <Link to="/tutor/settings">
        <p>My Profile</p>
      </Link>
      <Link to="/tutor/settings/change-user-password">
        <p>Change Password</p>
      </Link>
      <p onClick={handleLogOut}>Logout</p>
    </div>
  );
};

export default TopNav;
