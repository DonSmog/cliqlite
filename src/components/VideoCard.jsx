/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import "../styles/video-card.css";

import VideoItem from "./VideoItem";
import { baseUrl, getData } from "../apis/apiMethods";
import { apiEndpoints } from "../apis/apiEndpoints";
import MoonLoader from "react-spinners/MoonLoader";
import { v4 as uuid } from "uuid";
import { useAuth } from "../context/auth.context";

const VideoCard = ({ option1, option2 }) => {
  const { topicID, subjectID } = useAuth();
  const [video, setVideo] = useState([]);
  const [loading, setLoading] = useState(false);

  const getVideo = () => {
    setLoading(true);

    getData(
      baseUrl + apiEndpoints.TOPIC_BY_SUBJECT + `${subjectID}/${topicID}`
    ).then((res) => {
      setLoading(false);
      setVideo(res.data.data);
    });
  };

  useEffect(() => {
    getVideo();
  }, [topicID]);

  return (
    <div className="video-card">
      {loading ? (
        <div className="loading">
          <MoonLoader color="var(--button)" />
        </div>
      ) : (
        <div className="video-card-items">
          {video.map((item) => {
            const { name, video = {} } = item;
            const { duration, url } = video;
            return (
              <div key={uuid()} className="video-card_item">
                <VideoItem
                  name={name}
                  duration={duration}
                  url={url}
                  option1={option1}
                  option2={option2}
                />
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
};

export default VideoCard;
