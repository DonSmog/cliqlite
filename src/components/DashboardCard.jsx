import React from "react";
import "../styles/dashboard-card.css";

const DashboardCard = ({ children }) => {
  return <div className="dashboard-card">{children}</div>;
};

export default DashboardCard;
