import React from "react";
import "../styles/card.css";
import Logo from "../assets/Logo.svg";

const Card = ({ children, title, text, img, imgTitle }) => {
  return (
    <div className="card">
      <div className="logo">
        <img src={Logo} alt="Cliq Lite" />
      </div>
      <div className="card-image">
        <img src={img} alt={imgTitle} />
      </div>
      <h4>{title}</h4>
      <p>{text}</p>
      {children}
    </div>
  );
};

export default Card;
