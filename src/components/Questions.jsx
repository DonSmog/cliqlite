/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import "../styles/questions.css";
import QuestionsCard from "./QuestionsCard";
import { v4 as uuid } from "uuid";
// import { CgMathPlus } from "react-icons/cg";
import DashboardCard from "./DashboardCard";
import { useNavigate } from "react-router-dom";
import ROUTES from "../Helpers/routes";

const Questions = () => {
  const navigate = useNavigate();
  const [successModal, setSuccessModal] = useState(false);
  const [modalContent, setModalContent] = useState(null);
  // const [addQuestion] = useState(null);

  const [questions, setQuestions] = useState([
    <div key={uuid()}>
      <QuestionsCard />
    </div>,
  ]);

  useEffect(() => {
    const timer = setTimeout(() => {
      setSuccessModal(false);
    }, 3000);
    return () => clearTimeout(timer);
  }, [successModal]);

  // const handleAddQuestions = () => {
  //   setSuccessModal(true);
  //   setModalContent("Another Question Added");
  //   setQuestions((questions) => [...questions, addQuestion]);
  // };

  const seeAllQuestions = () => {
    navigate(ROUTES.VIEW_QUESTIONS);
  };

  return (
    <>
      <DashboardCard>
        <div className="add-topic_header">
          <div className="questions">
            {successModal ? (
              <div className="settings-modal">{modalContent}</div>
            ) : null}

            {questions.map(() => (
              <div key={uuid()}>{questions}</div>
            ))}
          </div>
        </div>
      </DashboardCard>

      <div className="add-question-button" onClick={seeAllQuestions}>
        <h5>See All Questions</h5>
      </div>

      {/* <div className="add-question-button" onClick={handleAddQuestions}>
        <CgMathPlus />
        <h5>Add another Question</h5>
      </div> */}
    </>
  );
};

export default Questions;
