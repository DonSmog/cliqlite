import React, { useState } from "react";
import "../styles/questions-modal.css";
import Modal from "react-bootstrap/Modal";
import ModalHeader from "react-bootstrap/ModalHeader";
import ModalTitle from "react-bootstrap/ModalTitle";
import ModalBody from "react-bootstrap/ModalBody";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { BsImage } from "react-icons/bs";
import Camera from "react-html5-camera-photo";
import "react-html5-camera-photo/build/css/index.css";
import PropTypes from "prop-types";

const QuestionsModal = ({ show, onHide, image, setImage }) => {
  const [dataUri, setDataUri] = useState("");

  function handleTakePhotoAnimationDone(dataUri) {
    setDataUri(dataUri);
  }

  const handleRestartCamera = () => {
    setDataUri("");
  };

  const handleFileChange = (e) => {
    const file = e.target.files[0];
    const url = URL.createObjectURL(file);
    setImage(url);
  };

  return (
    <div className="questions-modal">
      <Modal
        show={show}
        onHide={onHide}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <ModalHeader closeButton>
          <ModalTitle id="contained-modal-title-vcenter">
            Insert Image
          </ModalTitle>
        </ModalHeader>
        <ModalBody>
          <Tabs className="questions-modal-tabs">
            <TabList className="questions-modal-tablist">
              <Tab>UPLOAD</Tab>
              <Tab>CAMERA</Tab>
            </TabList>

            <TabPanel>
              <div className="questions-modal-upload add-topic_header-body">
                {image ? (
                  <div className="add-topic-image">
                    <label>
                      <input type="checkbox" />
                      <img src={image} alt="Topic" title="Topic" />
                    </label>
                    <label htmlFor="file" onClick={handleFileChange}>
                      <p>Change Cover Art</p>
                    </label>
                  </div>
                ) : (
                  <label htmlFor="file" id="modal-pictures">
                    <div className="add-topic_header-body_image-cover">
                      <BsImage />
                      <p>Upload cover art</p>
                    </div>
                  </label>
                )}

                <input
                  type="file"
                  accept=".png, .jpg, .jpeg"
                  id="file"
                  style={{ display: "none" }}
                  onChange={handleFileChange}
                />
                <label htmlFor="file" id="modal-button">
                  Browse
                </label>
              </div>
            </TabPanel>
            <TabPanel>
              {dataUri ? (
                <div className="start-camera">
                  <ImagePreview dataUri={dataUri} isFullscreen={false} />
                  <span onClick={handleRestartCamera}>Retake Image</span>
                </div>
              ) : (
                <Camera
                  onTakePhotoAnimationDone={handleTakePhotoAnimationDone}
                  isFullscreen={false}
                />
              )}
            </TabPanel>
          </Tabs>
        </ModalBody>
      </Modal>
    </div>
  );
};

const ImagePreview = ({ dataUri, isFullscreen }) => {
  let classNameFullscreen = isFullscreen ? "demo-image-preview-fullscreen" : "";

  return (
    <div className={"demo-image-preview " + classNameFullscreen}>
      <img src={dataUri} alt="dataUri" />
    </div>
  );
};

ImagePreview.propTypes = {
  dataUri: PropTypes.string,
  isFullscreen: PropTypes.bool,
};

export default QuestionsModal;
