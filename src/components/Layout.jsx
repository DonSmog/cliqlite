import React from "react";
import { Outlet } from "react-router-dom";
import Sidebar from "../components/Sidebar";
import TopNav from "../components/TopNav";
import "../styles/layout.css";
import { AuthProvider } from "../context/auth.context";

const Layout = () => {
  return (
    <AuthProvider>
      <div className="layout" id="scrollToTop">
        <Sidebar />
        <div className="layout_content">
          <div className="layout_content-main">
            <TopNav />
          </div>
          <div className="layout_content-main">
            <Outlet />
          </div>
        </div>
      </div>
    </AuthProvider>
  );
};

export default Layout;
