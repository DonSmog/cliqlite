/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import "../styles/quiz-card.css";

import { useNavigate } from "react-router-dom";
import QuizItem from "./QuizItem";
import Button from "./Button";
import ROUTES from "../Helpers/routes";
import { baseUrl, getData } from "../apis/apiMethods";
import { apiEndpoints } from "../apis/apiEndpoints";
import MoonLoader from "react-spinners/MoonLoader";
import { v4 as uuid } from "uuid";

const QuizCard = ({ option1, option2, option3 }) => {
  const navigate = useNavigate();
  const [quiz, setQuiz] = useState([]);
  const [loading, setLoading] = useState(false);

  const selectedId = localStorage.getItem("topicId");

  const getQuiz = () => {
    setLoading(true);

    getData(baseUrl + apiEndpoints.QUIZZES_BY_TOPIC + selectedId).then(
      (res) => {
        setLoading(false);
        setQuiz(res.data.data);
      }
    );
  };

  useEffect(() => {
    getQuiz();
  }, [selectedId]);

  const handleAddQuiz = () => {
    navigate(ROUTES.ADD_QUIZ);
  };

  return (
    <div className="quiz-card">
      {loading ? (
        <div className="loading">
          <MoonLoader color="var(--button)" />
        </div>
      ) : (
        <div>
          <div className="quiz-card-section">
            <div className="quiz-card-section_header">
              <h5>Mix and Match</h5>
              <p>See All</p>
            </div>
            <div key={uuid()} className="video-card-items2">
              {quiz.map((quiz) => {
                const { photo, title } = quiz;
                return (
                  <div key={uuid()}>
                    <QuizItem
                      option1={option1}
                      option2={option2}
                      option3={option3}
                      title={title}
                      photo={photo}
                    />
                  </div>
                );
              })}
            </div>
          </div>

          <Button handleSubmit={handleAddQuiz}>New Quiz</Button>
        </div>
      )}
    </div>
  );
};

export default QuizCard;
