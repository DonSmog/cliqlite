import React from "react";
import "../styles/courses-dropdown.css";

import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import VideoCard from "./VideoCard";
import QuizCard from "./QuizCard";
import Button from "./Button";
import ROUTES from "../Helpers/routes";

import { useNavigate } from "react-router-dom";

const CoursesDropdown = ({ topicId }) => {
  const navigate = useNavigate();

  const handleUploadVideo = () => {
    navigate(ROUTES.ADD_VIDEO);
  };

  return (
    <div className="courses-dropdown">
      <Tabs>
        <TabList className="courses-dropdown_tablist">
          <Tab>Videos</Tab>
          <Tab>Quiz</Tab>
        </TabList>

        <TabPanel>
          <VideoCard
            topicId={topicId}
            option1={"View Video"}
            option2={"Delete Video"}
          />
          <Button handleSubmit={handleUploadVideo}>Upload Video</Button>
        </TabPanel>
        <TabPanel>
          <QuizCard
            option1="View Quiz"
            option2="Add Questions"
            option3="Delete Quiz"
          />
        </TabPanel>
      </Tabs>
    </div>
  );
};

export default CoursesDropdown;
