import React from "react";
import "../styles/sidebar.css";
import Logo from "../assets/Logo.svg";
import { NavLink, useNavigate } from "react-router-dom";

import { TiHomeOutline } from "react-icons/ti";
import { RiLogoutBoxRLine } from "react-icons/ri";

import {
  BsCalendar4Event,
  BsCameraVideo,
  BsQuestionCircle,
} from "react-icons/bs";
import { FiSettings } from "react-icons/fi";
import { FaBasketballBall } from "react-icons/fa";
import ROUTES from "../Helpers/routes";
import { useAuth } from "../context/auth.context";

const SidebarItems = (props) => {
  return (
    <div className="sidebar_item">
      <div className="sidebar_item-inner">
        <p>{props.icon}</p>
        <span>{props.title}</span>
      </div>
    </div>
  );
};

const Menu = ({ onClick }) => {
  return (
    <>
      <div className="sidebar_logo">
        <img src={Logo} alt="logo" />
      </div>
      <NavLink to="/tutor/dashboard">
        <SidebarItems title="Assigned Classes" icon={<TiHomeOutline />} />
      </NavLink>
      <NavLink to="/tutor/schedule">
        <SidebarItems title="Schedule" icon={<BsCalendar4Event />} />
      </NavLink>
      <NavLink to="/tutor/quiz">
        <SidebarItems title="Quiz" icon={<FaBasketballBall />} />
      </NavLink>
      <NavLink to="/tutor/video">
        <SidebarItems title="Video" icon={<BsCameraVideo />} />
      </NavLink>
      <NavLink to="/tutor/feedback">
        <SidebarItems title="Feedback" icon={<BsQuestionCircle />} />
      </NavLink>
      <NavLink to="/tutor/settings">
        <SidebarItems title="Settings" icon={<FiSettings />} />
      </NavLink>

      <div className="sidebar-logout" onClick={onClick}>
        <SidebarItems title="Log Out" icon={<RiLogoutBoxRLine />} />
      </div>
    </>
  );
};

const Sidebar = () => {
  const navigate = useNavigate();
  const { logOut } = useAuth();

  const logout = () => {
    logOut();
    navigate(ROUTES.LOGIN);
  };

  return (
    <>
      <div className="sidebar">
        <Menu onClick={logout} />
      </div>
    </>
  );
};

export default Sidebar;
