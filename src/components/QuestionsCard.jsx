/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Select from "react-select";
import {
  RiCloseCircleLine,
  RiRadioButtonFill,
  RiDeleteBin6Line,
  RiImageLine,
} from "react-icons/ri";
import { BsPen } from "react-icons/bs";
import QuestionsModal from "./QuestionsModal";
import { v4 as uuid } from "uuid";
import { baseUrl, postData } from "../apis/apiMethods";
import Button from "./Button";
import { PulseLoader } from "react-spinners";

const QuestionsCard = () => {
  const [loading, setLoading] = useState(false);
  const [selectedRadioBtn, setSelectedRadioBtn] = useState("optionA");
  const [correctAnswer, setCorrectAnswer] = useState("optionA");
  const [blank, setBlank] = useState([<Options />]);
  const [addQuestion] = useState(null);
  const [modalShow, setModalShow] = useState(false);
  const [image, setImage] = useState("");
  const [isExplanation, setIsExplanation] = useState(false);
  const [successModal, setSuccessModal] = useState(false);
  const [modalContent, setModalContent] = useState(null);

  const options = [
    {
      value: "Multiple Choice",
      label: 1,
      icon: <RiRadioButtonFill />,
    },
    {
      value: "Fill in the Gaps",
      label: 2,
      icon: <BsPen />,
    },
  ];

  const [selectedOption, setSelectedOption] = useState(options[0]);

  const [newQuestion, setNewQuestion] = useState({
    description: "",
    questionNo: "",
    option1: "",
    option2: "",
    option3: "",
    option4: "",
    explanation: "",
  });

  const {
    description,
    questionNo,
    option1,
    option2,
    option3,
    option4,
    explanation,
  } = newQuestion;

  const isRadioSelected = (value) => selectedRadioBtn === value;

  const handleRadioClicked = (e) => {
    setSelectedRadioBtn(e.target.value);
  };

  const isCheckboxSelected = (value) => correctAnswer === value;

  const handleCheckboxClicked = (e) => {
    setCorrectAnswer(e.target.value);
  };

  const handleChange = (e) => {
    setSelectedOption(e);
  };

  const handleAddBlank = () => {
    setBlank((blank) => [...blank, addQuestion]);
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setNewQuestion({ ...newQuestion, [name]: value });
  };

  const onHide = (e) => {
    setModalShow(false);
    const file = e.target.files[0];
    const url = URL.createObjectURL(file);
    setImage(url);
    console.log(setImage(url));
  };

  const giveExplanation = () => {
    setIsExplanation(!isExplanation);
  };

  const removeImage = () => {
    setImage(null);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);

    const quizId = localStorage.getItem("quizId");

    const fd = new FormData();
    fd.append("description", description);
    fd.append("no", questionNo);
    fd.append("option1", option1);
    fd.append("option2", option2);
    fd.append("option3", option3);
    fd.append("option4", option4);
    fd.append("correctAnswer", correctAnswer);
    fd.append("explanation", explanation);
    fd.append("image", image);

    if (!questionNo) {
      setLoading(false);
      setSuccessModal(true);
      setModalContent("Please input a Question No.");
    } else if (!description) {
      setLoading(false);
      setSuccessModal(true);
      setModalContent("Please enter a Question");
    } else if (!correctAnswer) {
      setLoading(false);
      setSuccessModal(true);
      setModalContent("Please select a correct answer");
    } else {
      postData(baseUrl + `questions/${quizId}`, fd)
        .then((res) => {
          if (res.status === 201) {
            setLoading(false);
            setSuccessModal(true);
            setModalContent(
              "Question added successfully. You can add another Question"
            );
            setImage(null);
            setCorrectAnswer("optionA");
            setSelectedRadioBtn("Option A");
            setNewQuestion({
              description: "",
              questionNo: "",
              option1: "",
              option2: "",
              option3: "",
              option4: "",
              explanation: "",
            });
          }
        })
        .catch((err) => {
          setLoading(false);
          setSuccessModal(true);
          setModalContent("Error adding Question");
          setImage(null);
          setCorrectAnswer("optionA");
          setSelectedRadioBtn("Option A");
          setNewQuestion({
            description: "",
            questionNo: "",
            option1: "",
            option2: "",
            option3: "",
            option4: "",
            explanation: "",
          });
        });
    }
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      setSuccessModal(false);
    }, 3000);
    return () => clearTimeout(timer);
  }, [successModal]);

  return (
    <form onSubmit={handleSubmit}>
      {successModal ? (
        <div className="settings-modal">{modalContent}</div>
      ) : null}
      <div className="questions-header">
        <div>
          <h5>Question </h5>
          <input
            type="text"
            name="questionNo"
            maxLength={3}
            value={questionNo}
            onChange={handleInputChange}
            placeholder="No."
          />
        </div>
        <div className="questions-line">
          <span></span>
        </div>
      </div>
      <QuestionsModal
        setImage={setImage}
        image={image}
        show={modalShow}
        onHide={onHide}
      />
      <div className="questions-body">
        {image ? (
          <div className="optional-image">
            <label>
              <input type="checkbox" />
              <img src={image} alt="Topic" title="Topic" />
            </label>
            <p onClick={removeImage}>Remove Image</p>
          </div>
        ) : null}
        <div className="questions-body-head">
          <input
            type="text"
            name="description"
            value={description}
            onChange={handleInputChange}
            placeholder="What is the name of a Flying Fish?"
            className="questions-description"
          />
          <div
            className="questions-body-image"
            onClick={() => {
              setModalShow(true);
            }}
          >
            <RiImageLine />
          </div>

          <Select
            options={options}
            value={selectedOption}
            onChange={handleChange}
            className="react-select-questions-container"
            classNamePrefix="react-select"
            getOptionLabel={(e) => (
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  color: "#016F54",
                }}
              >
                <span>{e.icon}</span>
                <span
                  style={{
                    marginLeft: 10,
                    color: "#333333",
                    fontFamily: "var(--font-family)",
                  }}
                >
                  {e.value}
                </span>
              </div>
            )}
          />
        </div>

        {selectedOption.label === 1 ? (
          <div className="questions-body-options">
            <h2>OPTIONS</h2>
            <div className="questions-body-option">
              <div>
                <input
                  type="radio"
                  value="optionA"
                  checked={isRadioSelected("optionA")}
                  onChange={handleRadioClicked}
                />
                <input
                  type="text"
                  name="option1"
                  value={option1}
                  onChange={handleInputChange}
                  placeholder="Option A?"
                  className="questions-options"
                />
                <input
                  type="checkbox"
                  value="optionA"
                  checked={isCheckboxSelected("optionA")}
                  onChange={handleCheckboxClicked}
                />{" "}
                <h5>Correct Option</h5>
              </div>
              <RiCloseCircleLine />
            </div>
            <div className="questions-body-option">
              <div>
                <input
                  type="radio"
                  value="optionB"
                  checked={isRadioSelected("optionB")}
                  onChange={handleRadioClicked}
                />
                <input
                  type="text"
                  name="option2"
                  value={option2}
                  onChange={handleInputChange}
                  placeholder="Option B?"
                  className="questions-options"
                />
                <input
                  type="checkbox"
                  value="optionB"
                  checked={isCheckboxSelected("optionB")}
                  onChange={handleCheckboxClicked}
                />{" "}
                <h5>Correct Option</h5>
              </div>
              <RiCloseCircleLine />
            </div>
            <div className="questions-body-option">
              <div>
                <input
                  type="radio"
                  value="optionC"
                  checked={isRadioSelected("optionC")}
                  onChange={handleRadioClicked}
                />
                <input
                  type="text"
                  name="option3"
                  value={option3}
                  onChange={handleInputChange}
                  placeholder="Option C?"
                  className="questions-options"
                />
                <input
                  type="checkbox"
                  value="optionC"
                  checked={isCheckboxSelected("optionC")}
                  onChange={handleCheckboxClicked}
                />{" "}
                <h5>Correct Option</h5>
              </div>
              <RiCloseCircleLine />
            </div>
            <div className="questions-body-option">
              <div>
                <input
                  type="radio"
                  value="optionD"
                  checked={isRadioSelected("optionD")}
                  onChange={handleRadioClicked}
                />
                <input
                  type="text"
                  name="option4"
                  value={option4}
                  onChange={handleInputChange}
                  placeholder="Option D?"
                  className="questions-options"
                />
                <input
                  type="checkbox"
                  value="optionD"
                  checked={isCheckboxSelected("optionD")}
                  onChange={handleCheckboxClicked}
                />{" "}
                <h5>Correct Option</h5>
              </div>
              <RiCloseCircleLine />
            </div>
            <div className="explanation">
              {isExplanation ? (
                <div className="explanation-body">
                  <textarea
                    className="video-title-description"
                    name="explanation"
                    value={explanation}
                    placeholder="Give Detailed Explanation to justify the answer. This is optional"
                    onChange={handleInputChange}
                  />
                  <div onClick={giveExplanation}>
                    <RiCloseCircleLine />
                  </div>
                </div>
              ) : (
                <p onClick={giveExplanation}>Give Explanation</p>
              )}
            </div>
          </div>
        ) : (
          <div className="questions-body-options">
            {blank.map((_, i) => (
              <div key={uuid()}>{blank}</div>
            ))}
            <p onClick={handleAddBlank}>Add Blank</p>

            <h2>OPTIONS</h2>
            <div className="questions-body-option">
              <div>
                <input
                  type="radio"
                  value="optionA"
                  checked={isRadioSelected("optionA")}
                  onChange={handleRadioClicked}
                />
                <input
                  type="text"
                  name="option1"
                  value={option1}
                  onChange={handleInputChange}
                  placeholder="Option A"
                  className="questions-options"
                />
                <input
                  type="checkbox"
                  value="optionA"
                  checked={isCheckboxSelected("optionA")}
                  onChange={handleCheckboxClicked}
                />
                <h5>Correct Option</h5>
              </div>
              <RiCloseCircleLine />
            </div>
            <div className="questions-body-option">
              <div>
                <input
                  type="radio"
                  value="optionB"
                  checked={isRadioSelected("optionB")}
                  onChange={handleRadioClicked}
                />
                <input
                  type="text"
                  name="option1"
                  value={option1}
                  onChange={handleInputChange}
                  placeholder="Option B"
                  className="questions-options"
                />
                <input
                  type="checkbox"
                  value="optionB"
                  checked={isCheckboxSelected("optionB")}
                  onChange={handleCheckboxClicked}
                />{" "}
                <h5>Correct Option</h5>
              </div>
              <RiCloseCircleLine />
            </div>
            <div className="questions-body-option">
              <div>
                <input
                  type="radio"
                  value="optionC"
                  checked={isRadioSelected("optionC")}
                  onChange={handleRadioClicked}
                />
                <input
                  type="text"
                  name="option2"
                  value={option2}
                  onChange={handleInputChange}
                  placeholder="Option C"
                  className="questions-options"
                />
                <input
                  type="checkbox"
                  value="optionC"
                  checked={isCheckboxSelected("optionC")}
                  onChange={handleCheckboxClicked}
                />{" "}
                <h5>Correct Option</h5>
              </div>
              <RiCloseCircleLine />
            </div>
            <div className="questions-body-option">
              <div>
                <input
                  type="radio"
                  value="optionD"
                  checked={isRadioSelected("optionD")}
                  onChange={handleRadioClicked}
                />
                <input
                  type="text"
                  name="option4"
                  value={option4}
                  onChange={handleInputChange}
                  placeholder="Option D"
                  className="questions-options"
                />
                <input
                  type="checkbox"
                  value="optionD"
                  checked={isCheckboxSelected("optionD")}
                  onChange={handleCheckboxClicked}
                />{" "}
                <h5>Correct Option</h5>
              </div>
              <RiCloseCircleLine />
            </div>
            <div className="explanation">
              {isExplanation ? (
                <div className="explanation-body">
                  <textarea
                    className="video-title-description"
                    name="explanation"
                    value={explanation}
                    placeholder="Give Detailed Explanation to justify the answer"
                    onChange={handleInputChange}
                  />
                  <div onClick={giveExplanation}>
                    <RiCloseCircleLine />
                  </div>
                </div>
              ) : (
                <p onClick={giveExplanation}>Give Explanation</p>
              )}
            </div>
          </div>
        )}

        <div className="questions-bottom">
          <div className="questions-line-bottom">
            <span></span>
          </div>
          <div className="questions-delete-btn">
            <RiDeleteBin6Line />
          </div>
        </div>
        <Button disabled={loading}>
          {loading ? <PulseLoader size="10px" color="#b4c304" /> : "Save"}
        </Button>
      </div>
    </form>
  );
};

const Options = () => (
  <div className="questions-body-optionB">
    <div>
      <input
        type="text"
        placeholder="Insert text"
        className="questions-options"
      />
    </div>
    <RiCloseCircleLine />
  </div>
);

export default QuestionsCard;
