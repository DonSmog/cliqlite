import "./styles/App.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { AuthProvider } from "./context/auth.context";
import ROUTES from "./Helpers/routes";
import ScrollToTop from "./Helpers/ScrollToTop";
import LandingPage from "./pages";
import Login from "./pages/Login";
import Signup from "./pages/Signup";
import ForgetPassword from "./pages/ForgetPassword";
import ConfirmForgetPassword from "./pages/ConfirmForgetPassword";
import ChangePassword from "./pages/ChangePassword";
import PasswordChangeSuccess from "./pages/PasswordChangeSuccess";
import Layout from "./components/Layout";
import Schedule from "./pages/Dashboard/Schedule";
import Quiz from "./pages/Dashboard/Quiz/Quiz";
import Video from "./pages/Dashboard/Video/Video";
import Feedback from "./pages/Dashboard/Feedback";
import Settings from "./pages/Dashboard/Settings/Settings";
import ChangeUserPassword from "./pages/Dashboard/Settings/ChangeUserPassword";
import Classes from "./pages/Dashboard/Class/Classes";
import ViewCourses from "./pages/Dashboard/Class/ViewCourses";
import AddTopics from "./pages/Dashboard/Class/AddTopics";
import AddVideo from "./pages/Dashboard/Video/AddVideo";
import AddQuiz from "./pages/Dashboard/Quiz/AddQuiz";
import AllVideos from "./pages/Dashboard/Video/AllVideos";
import AddQuizQuestions from "./pages/Dashboard/Quiz/AddQuizQuestions";
import ViewQuiz from "./pages/Dashboard/Quiz/ViewQuiz";
import PlayQuizGame from "./pages/PlayQuizGame";
import PlayerInfo from "./pages/PlayerInfo";

function App() {
  return (
    <Router>
      <AuthProvider>
        <Routes>
          <Route path={ROUTES.HOME} element={<LandingPage />} />
          <Route path={ROUTES.PLAYER_INFO} element={<PlayerInfo />} />
          <Route path={ROUTES.PLAY_GAME} element={<PlayQuizGame />} />
          <Route path={ROUTES.LOGIN} element={<Login />} />
          <Route path={ROUTES.SIGNUP} element={<Signup />} />
          <Route path={ROUTES.FORGET_PASSWORD} element={<ForgetPassword />} />
          <Route path={ROUTES.CHANGE_PASSWORD} element={<ChangePassword />} />
          <Route
            path={ROUTES.CONFIRM_FORGET_PASSWORD}
            element={<ConfirmForgetPassword />}
          />
          <Route
            path={ROUTES.PASSWORD_CHANGE_SUCCESS}
            element={<PasswordChangeSuccess />}
          />
          <Route path={ROUTES.AUTH} element={<Layout />}>
            <Route path={ROUTES.DASHBOARD} element={<Classes />} />
            <Route path={ROUTES.VIEW_COURSES} element={<ViewCourses />} />
            <Route path={ROUTES.ADD_TOPIC} element={<AddTopics />} />
            <Route path={ROUTES.SCHEDULE} element={<Schedule />} />
            <Route path={ROUTES.QUIZ} element={<Quiz />} />
            <Route path={ROUTES.ADD_QUIZ} element={<AddQuiz />} />
            <Route path={ROUTES.ADD_QUESTIONS} element={<AddQuizQuestions />} />
            <Route path={ROUTES.VIEW_QUESTIONS} element={<ViewQuiz />} />
            <Route path={ROUTES.VIDEO} element={<Video />} />
            <Route path={ROUTES.ALL_VIDEO} element={<AllVideos />} />
            <Route path={ROUTES.ADD_VIDEO} element={<AddVideo />} />
            <Route path={ROUTES.FEEDBACK} element={<Feedback />} />
            <Route path={ROUTES.SETTINGS} element={<Settings />} />
            <Route
              path={ROUTES.CHANGE_USER_PASSWORD}
              element={<ChangeUserPassword />}
            />
          </Route>
        </Routes>
      </AuthProvider>
      <ScrollToTop />
    </Router>
  );
}

export default App;
