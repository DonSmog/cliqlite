import { createContext, useEffect, useState, useContext } from "react";
// import { encrypt, decrypt } from "../apis/apiMethods";

// Create the context
export const AuthContext = createContext({
  userDetails: {
    id: 0,
    name: "",
    email: "",
    password: "",
    role: "",
    isAuthenticated: false,
  },
  setUserDetails: (userDetails) => userDetails,
  token: "",
  setUserToken: (token) => token,
  logOut: () => null,
});

export const AuthProvider = ({ children }) => {
  const [userDetails, setDetails] = useState({
    id: 0,
    name: "",
    email: "",
    password: "",
    role: "",
    isAuthenticated: false,
  });
  const [token, setToken] = useState("");
  const [profilePicture, setProfilePicture] = useState(null);

  useEffect(() => {
    const user = localStorage.getItem("user");
    const userToken = localStorage.getItem("userToken");
    if (user && userToken) {
      setDetails(user);
      setToken(userToken);
    }
  }, []);

  const setUserToken = (userToken) => {
    localStorage.setItem("userToken", userToken);
    setToken(userToken);
  };

  const setUserDetails = (user) => {
    localStorage.setItem("user", user);
    setDetails(user);
  };

  const logOut = () => {
    localStorage.clear();
    setDetails({
      id: 0,
      name: "",
      email: "",
      password: "",
      role: "",
      isAuthenticated: false,
    });
    setToken("");
  };

  const [subjectID, setSubjectID] = useState("");
  const [topicID, setTopicID] = useState("");

  return (
    <AuthContext.Provider
      value={{
        userDetails,
        setUserDetails,
        token,
        setUserToken,
        logOut,
        profilePicture,
        setProfilePicture,
        subjectID,
        setSubjectID,
        topicID,
        setTopicID,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

// Custom Hook
export const useAuth = () => useContext(AuthContext);
