const data = [
  {
    id: 1,
    questionText: "How many colors are there in a rainbow?",
    correctAnswer: "7",
    answerOptions: [
      {
        option: "A",
        answerText: "4",
        isCorrect: false,
      },
      {
        option: "B",
        answerText: "7",
        isCorrect: true,
      },
      {
        option: "C",
        answerText: "5",
        isCorrect: false,
      },
      {
        option: "D",
        answerText: "9",
        isCorrect: false,
      },
    ],
  },
  {
    id: 2,
    questionText: "What does a thermometer measure?",
    correctAnswer: "Temperature",
    answerOptions: [
      {
        option: "A",
        answerText: "heat",
        isCorrect: false,
      },
      {
        option: "B",
        answerText: "wind",
        isCorrect: false,
      },
      {
        option: "C",
        answerText: "cold",
        isCorrect: false,
      },
      {
        option: "D",
        answerText: "temperature",
        isCorrect: true,
      },
    ],
  },
  {
    id: 3,
    questionText: "What fruit do raisins come from?",
    correctAnswer: "Grapes",
    answerOptions: [
      {
        option: "A",
        answerText: "grapes",
        isCorrect: true,
      },
      {
        option: "B",
        answerText: "strawberry",
        isCorrect: false,
      },
      {
        option: "C",
        answerText: "pomegranates",
        isCorrect: false,
      },
      {
        option: "D",
        answerText: "corn",
        isCorrect: false,
      },
    ],
  },
  {
    id: 4,
    questionText: "How many wheels does a tricycle have?",
    correctAnswer: "3",
    answerOptions: [
      {
        option: "A",
        answerText: "6",
        isCorrect: false,
      },
      {
        option: "B",
        answerText: "3",
        isCorrect: true,
      },
      {
        option: "C",
        answerText: "9",
        isCorrect: false,
      },
      {
        option: "D",
        answerText: "7",
        isCorrect: false,
      },
    ],
  },
  {
    id: 5,
    questionText: "Who filmed the first video in space?",
    correctAnswer: "Chris Hadfield",
    answerOptions: [
      {
        option: "A",
        answerText: "Bruno Mars",
        isCorrect: false,
      },
      {
        option: "B",
        answerText: "Chris Hadfield",
        isCorrect: true,
      },
      {
        option: "C",
        answerText: "Michael Jackson",
        isCorrect: false,
      },
      {
        option: "D",
        answerText: "Elvis Presley",
        isCorrect: false,
      },
    ],
  },
  {
    id: 6,
    questionText: "How many toes does a cat have?",
    correctAnswer: "18",
    answerOptions: [
      {
        option: "A",
        answerText: "15",
        isCorrect: false,
      },
      {
        option: "B",
        answerText: "18",
        isCorrect: true,
      },
      {
        option: "C",
        answerText: "10",
        isCorrect: false,
      },
      {
        option: "D",
        answerText: "9",
        isCorrect: false,
      },
    ],
  },
  {
    id: 7,
    questionText: "What is the hardest part of your body?",
    correctAnswer: "Teeth",
    answerOptions: [
      {
        option: "A",
        answerText: "bone",
        isCorrect: false,
      },
      {
        option: "B",
        answerText: "teeth",
        isCorrect: true,
      },
      {
        option: "C",
        answerText: "skull",
        isCorrect: false,
      },
      {
        option: "D",
        answerText: "none of the above",
        isCorrect: false,
      },
    ],
  },
  {
    id: 8,
    questionText: "How many legs does a spider have?",
    correctAnswer: "8",
    answerOptions: [
      {
        option: "A",
        answerText: "7",
        isCorrect: false,
      },
      {
        option: "B",
        answerText: "9",
        isCorrect: false,
      },
      {
        option: "C",
        answerText: "8",
        isCorrect: true,
      },
      {
        option: "D",
        answerText: "6",
        isCorrect: false,
      },
    ],
  },
  {
    id: 9,
    questionText: "When was the first iPad released?",
    correctAnswer: "2010",
    answerOptions: [
      {
        option: "A",
        answerText: "2000",
        isCorrect: false,
      },
      {
        option: "B",
        answerText: "2018",
        isCorrect: false,
      },
      {
        option: "C",
        answerText: "2015",
        isCorrect: false,
      },
      {
        option: "D",
        answerText: "2010",
        isCorrect: true,
      },
    ],
  },
  {
    id: 10,
    questionText: "What sounds can you hear in space",
    correctAnswer: "None",
    answerOptions: [
      {
        option: "A",
        answerText: "loud sounds",
        isCorrect: false,
      },
      {
        option: "B",
        answerText: "low sounds",
        isCorrect: false,
      },
      {
        option: "C",
        answerText: "none",
        isCorrect: true,
      },
      {
        option: "D",
        answerText: "everything",
        isCorrect: false,
      },
    ],
  },
  {
    id: 11,
    questionText: "Everything is made from what?",
    correctAnswer: "Elements and Atoms",
    answerOptions: [
      {
        option: "A",
        answerText: "matter",
        isCorrect: false,
      },
      {
        option: "B",
        answerText: "elements and atoms",
        isCorrect: true,
      },
      {
        option: "C",
        answerText: "atoms",
        isCorrect: false,
      },
      {
        option: "D",
        answerText: "particles",
        isCorrect: false,
      },
    ],
  },
  {
    id: 12,
    questionText: "How many planets are in our solar system?",
    correctAnswer: "8",
    answerOptions: [
      {
        option: "A",
        answerText: "8",
        isCorrect: true,
      },
      {
        option: "B",
        answerText: "9",
        isCorrect: false,
      },
      {
        option: "C",
        answerText: "7",
        isCorrect: false,
      },
      {
        option: "D",
        answerText: "6",
        isCorrect: false,
      },
    ],
  },
  {
    id: 13,
    questionText: "What do caterpillars turn into?",
    correctAnswer: "Butterfly",
    answerOptions: [
      {
        option: "A",
        answerText: "moth",
        isCorrect: false,
      },
      {
        option: "B",
        answerText: "beetle",
        isCorrect: false,
      },
      {
        option: "C",
        answerText: "butterfly",
        isCorrect: true,
      },
      {
        option: "D",
        answerText: "dragon fly",
        isCorrect: false,
      },
    ],
  },
  {
    id: 14,
    questionText:
      "If you suffer from arachnophobia, which animal are you scared of?",
    correctAnswer: "Spiders",
    answerOptions: [
      {
        option: "A",
        answerText: "ants",
        isCorrect: false,
      },
      {
        option: "B",
        answerText: "spiders",
        isCorrect: true,
      },
      {
        option: "C",
        answerText: "cockroaches",
        isCorrect: false,
      },
      {
        option: "D",
        answerText: "mosquitoes",
        isCorrect: false,
      },
    ],
  },
  {
    id: 15,
    questionText: "What is the largest organ on the human body?",
    correctAnswer: "Skin",
    answerOptions: [
      {
        option: "A",
        answerText: "heart",
        isCorrect: false,
      },
      {
        option: "B",
        answerText: "brain",
        isCorrect: false,
      },
      {
        option: "C",
        answerText: "Skin",
        isCorrect: true,
      },
      {
        option: "D",
        answerText: "Lungs",
        isCorrect: false,
      },
    ],
  },
  {
    id: 16,
    questionText: "What do you get when you boil water?",
    correctAnswer: "Steam",
    answerOptions: [
      {
        option: "A",
        answerText: "vapour",
        isCorrect: false,
      },
      {
        option: "B",
        answerText: "smoke",
        isCorrect: false,
      },
      {
        option: "C",
        answerText: "steam",
        isCorrect: true,
      },
      {
        option: "D",
        answerText: "gases",
        isCorrect: false,
      },
    ],
  },
  {
    id: 17,
    questionText: "What is the name of molten rock after a volcanic eruption?",
    correctAnswer: "Lava",
    answerOptions: [
      {
        option: "A",
        answerText: "magma",
        isCorrect: false,
      },
      {
        option: "B",
        answerText: "lava",
        isCorrect: true,
      },
      {
        option: "C",
        answerText: "volcano",
        isCorrect: false,
      },
      {
        option: "D",
        answerText: "hot rocks",
        isCorrect: false,
      },
    ],
  },
  {
    id: 18,
    questionText: "What natural phenomenon usually comes after a rainstorm?",
    correctAnswer: "Rainbow",
    answerOptions: [
      {
        option: "A",
        answerText: "sunshine",
        isCorrect: false,
      },
      {
        option: "B",
        answerText: "rainbow",
        isCorrect: true,
      },
      {
        option: "C",
        answerText: "lightning",
        isCorrect: false,
      },
      {
        option: "D",
        answerText: "Cloudy skies",
        isCorrect: false,
      },
    ],
  },
];

export default data;
