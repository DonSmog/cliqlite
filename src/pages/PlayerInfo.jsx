import React, { useEffect, useState } from "react";
import "../styles/player-info.css";
import { PulseLoader } from "react-spinners";
import Button from "../components/Button";
import Select from "react-select";
import { useNavigate } from "react-router-dom";
import ROUTES from "../Helpers/routes";
import { baseUrl, postData } from "../apis/apiMethods";
import { apiEndpoints } from "../apis/apiEndpoints";

import background_bt from "../assets/background_bt.svg";
import background_top from "../assets/background_top.svg";
import sign_up from "../assets/signup.svg";
import Card2 from "../components/card/Card2";

const PlayerInfo = () => {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const [modalContent, setModalContent] = useState("");
  const [nameModal, setNameModal] = useState(false);
  const [emailModal, setEmailModal] = useState(false);
  const [phoneModal, setPhoneModal] = useState(false);
  const [gradeModal, setGradeModal] = useState(false);
  const [gradeOption, setGradeOption] = useState("");
  const [user, setUser] = useState({
    name: "",
    phone: "",
    email: "",
  });

  const { name, phone, email } = user;

  const options = [
    { value: "6203fd774cba1d1a94eb06fe", label: "Grade 1" },
    { value: "6205227004d7d974edf6175a", label: "Grade 2" },
    { value: "6205227e04d7d974edf6175e", label: "Grade 3" },
    { value: "6205227e04d7d974edf6174e", label: "Grade 4" },
    { value: "6205227e04d7d974edf6178e", label: "Grade 5" },
  ];

  const handleClassChange = (e) => {
    setGradeOption(e);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    setLoading(true);
    const data = {
      name: name,
      email: email,
      phone: phone,
      grade: gradeOption.value,
    };

    if (name === "") {
      setLoading(false);
      setNameModal(true);
      setModalContent("Please input your name");
      return;
    } else if (email === "") {
      setModalContent("Please input your email address");
      setEmailModal(true);
      setLoading(false);
      return;
    } else if (!email.includes("@") || !email.includes(".")) {
      setLoading(false);
      setEmailModal(true);
      setModalContent("Please input a valid email address");
      return;
    } else if (phone === "") {
      setLoading(false);
      setModalContent("Please input your phone number");
      setPhoneModal(true);
      return;
    } else if (phone.length < 11) {
      setLoading(false);
      setModalContent("Phone number must be 11 digits");
      setPhoneModal(true);
      return;
    } else if (gradeOption === "") {
      setLoading(false);
      setModalContent("Please select your grade");
      setGradeModal(true);
      return;
    } else {
      postData(baseUrl + apiEndpoints.WAITLIST, data)
        .then((res) => {
          if (res.status === 201) {
            setLoading(false);
            navigate(ROUTES.PLAY_GAME);
            setUser({
              name: "",
              phone: "",
              email: "",
            });
            setGradeOption("");
          }
        })
        .catch((err) => {
          setLoading(false);
          setModalContent("Please fill the form again");
          setEmailModal(true);
          setUser({
            name: "",
            phone: "",
            email: "",
          });
          setGradeOption("");
        });
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUser({ ...user, [name]: value });
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      setEmailModal(false);
      setNameModal(false);
      setPhoneModal(false);
      setGradeModal(false);
    }, 3000);
    return () => clearTimeout(timer);
  }, [emailModal, nameModal, phoneModal, gradeModal]);

  return (
    <div className="player-info">
      <img src={background_top} alt="background" className="background_top" />
      <img src={background_bt} alt="background" className="background_bt" />
      <div className="player-info_content">
        <Card2
          title="Hey Champ!"
          text="Please fill in the form to start playing"
        >
          <div className="player-info-form">
            <form onSubmit={handleSubmit}>
              <div className="form-group2">
                {nameModal ? (
                  <div className="login-modal">{modalContent}</div>
                ) : null}
                <input
                  name="name"
                  type="text"
                  placeholder="Your Full Name"
                  value={name}
                  onChange={handleChange}
                  autoFocus
                />
              </div>

              <div className="form-group2">
                {emailModal ? (
                  <div className="login-modal">{modalContent}</div>
                ) : null}
                <input
                  name="email"
                  type="email"
                  placeholder="Your Email Address"
                  value={email}
                  onChange={handleChange}
                />
              </div>

              <div className="form-group2">
                {phoneModal ? (
                  <div className="login-modal">{modalContent}</div>
                ) : null}
                <input
                  name="phone"
                  maxLength={11}
                  type="tel"
                  pattern="[0-9]{11}"
                  placeholder="Your Phone Number"
                  value={phone}
                  onChange={handleChange}
                />
              </div>

              <div className="form-group2">
                {gradeModal ? (
                  <div className="login-modal">{modalContent}</div>
                ) : null}
                <Select
                  options={options}
                  defaultValue={gradeOption}
                  placeholder="Select your Grade Level"
                  onChange={handleClassChange}
                  menuPlacement="top"
                  className="react-select-container2"
                  classNamePrefix="react-select2"
                />
              </div>

              <Button disabled={loading}>
                {loading ? (
                  <PulseLoader size="10px" color="#fff" />
                ) : (
                  "Let's Play"
                )}
              </Button>
            </form>
          </div>
        </Card2>
        <img className="sign-up_image" src={sign_up} alt="Sign Up" />
      </div>
    </div>
  );
};

export default PlayerInfo;
