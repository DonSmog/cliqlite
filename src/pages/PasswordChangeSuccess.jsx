import React from "react";
import "../styles/login-success.css";
import Card from "../components/Card";
import Button from "../components/Button";
import { useNavigate } from "react-router-dom";
import Success from "../assets/success.svg";
import Background1 from "../assets/Background2.svg";
import Background2 from "../assets/Background1.svg";
import ROUTES from "../Helpers/routes";

const PasswordChangeSuccess = () => {
  let navigate = useNavigate();

  const handleSubmit = () => {
    navigate(ROUTES.LOGIN);
  };
  return (
    <div className="login-success">
      <Card
        img={Success}
        imgTitle="Success"
        title="SUCCESS!"
        text="Your password has been changed successfully!"
      >
        <Button handleSubmit={handleSubmit}>Proceed to Login</Button>
      </Card>

      <div className="background1">
        <img src={Background1} alt="background" />
      </div>
      <div className="background2">
        <img src={Background2} alt="background" />
      </div>
      <div className="line">
        <span></span>
      </div>
    </div>
  );
};

export default PasswordChangeSuccess;
