/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import "../styles/login.css";
import Card from "../components/Card";
import Button from "../components/Button";
import Background1 from "../assets/Background2.svg";
import Background2 from "../assets/Background1.svg";
import { FiEyeOff, FiEye } from "react-icons/fi";
import { Link, useNavigate } from "react-router-dom";
import { baseUrl } from "../apis/apiMethods";
import { apiEndpoints } from "../apis/apiEndpoints";
import ROUTES from "../Helpers/routes";
import { useAuth } from "../context/auth.context";
import { postData } from "../apis/apiMethods";
import PulseLoader from "react-spinners/PulseLoader";

const Login = () => {
  useEffect(() => {
    localStorage.clear();
  }, []);

  const { setUserDetails, setUserToken } = useAuth();
  const [userCredentials, setUserCredentials] = useState({
    email: "",
    password: "",
  });
  const { email, password } = userCredentials;
  const [showPassword, setShowPassword] = useState(false);
  const [emailModal, setEmailModal] = useState(false);
  const [passwordModal, setPasswordModal] = useState(false);
  const [modalContent, setModalContent] = useState(null);
  const [loading, setLoading] = useState(false);
  let navigate = useNavigate();

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleChange = (e) => {
    const { value, name } = e.target;

    setUserCredentials({ ...userCredentials, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    setLoading(true);
    const data = {
      email: email,
      password: password,
    };

    if (email === "") {
      setLoading(false);
      setModalContent("Please input your email address");
      setEmailModal(true);
      return;
    } else if (!email.includes("@") || !email.includes(".")) {
      setLoading(false);
      setEmailModal(true);
      setModalContent("Please input a valid email address");
      return;
    } else if (password === "") {
      setLoading(false);
      setPasswordModal(true);
      setModalContent("Please input your password");
      return;
    } else if (password.length < 6) {
      setLoading(false);
      setPasswordModal(true);
      setModalContent("Password must be at least 6 characters");
      return;
    } else {
      postData(baseUrl + apiEndpoints.LOGIN, data)
        .then((res) => {
          if (res.status === 200) {
            setLoading(false);
            setUserDetails(e.target.email.value);
            setUserToken(res.data.data);
            navigate(ROUTES.DASHBOARD);
          }
        })
        .catch((err) => {
          setLoading(false);
          setModalContent("Please check your email and password");
          setEmailModal(true);
        });
    }
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      setEmailModal(false);
      setPasswordModal(false);
    }, 3000);
    return () => clearTimeout(timer);
  }, [emailModal, passwordModal]);

  return (
    <div className="login">
      <Card title="Login to your Dashboard">
        <div className="login-form">
          <form onSubmit={handleSubmit}>
            <div className="form-group">
              {emailModal ? (
                <div className="login-modal">{modalContent}</div>
              ) : null}
              <input
                name="email"
                type="email"
                placeholder="Email Address"
                value={email}
                onChange={handleChange}
              />
            </div>
            <div className="form-group">
              {passwordModal ? (
                <div className="login-modal">{modalContent}</div>
              ) : null}
              <input
                name="password"
                placeholder="Password"
                type={showPassword ? "text" : "password"}
                id="password"
                onChange={handleChange}
                value={password}
              />
              <div className="eyes" onClick={handleClickShowPassword}>
                {showPassword ? <FiEyeOff /> : <FiEye />}
              </div>
            </div>
            <div className="forget-password-link">
              <Link to="/forget-password">
                <p>Forget Password?</p>
              </Link>
            </div>
            <Button disabled={loading}>
              {loading ? <PulseLoader size="10px" color="#b4c304" /> : "Log In"}
            </Button>
          </form>
        </div>
      </Card>

      <div className="background1">
        <img src={Background1} alt="background" />
      </div>
      <div className="background2">
        <img src={Background2} alt="background" />
      </div>
      <div className="line">
        <span></span>
      </div>
    </div>
  );
};

export default Login;
