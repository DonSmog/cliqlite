import React from "react";
import "../../styles/schedule.css";

import DashboardCard from "../../components/DashboardCard";

const Schedule = () => {
  return (
    <div className="schedule">
      <div className="schedule_header">
        <h2>Schedule</h2>
      </div>
      <DashboardCard>
        Get opinion on what to use. Either Google Calendar Api or Kalend package
      </DashboardCard>
    </div>
  );
};

export default Schedule;
