/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import "../../../styles/settings.css";

import DashboardCard from "../../../components/DashboardCard";
import Button from "../../../components/Button";
import Select from "react-select";
import { BsUpload } from "react-icons/bs";
import { useAuth } from "../../../context/auth.context";
import { patchData, baseUrl } from "../../../apis/apiMethods";
import { apiEndpoints } from "../../../apis/apiEndpoints";
import PulseLoader from "react-spinners/PulseLoader";

const imageUrl =
  "https://www.uni-giessen.de/international-pages/vip/support/ementors/male/image_preview";

const Settings = () => {
  const [subjectOption, setSubjectOption] = useState(null);
  const [classOption, setClassOption] = useState(null);
  const [successModal, setSuccessModal] = useState(false);
  const [modalContent, setModalContent] = useState(null);
  const [loading, setLoading] = useState(false);
  const { userDetails, setUserDetails, profilePicture, setProfilePicture } =
    useAuth();

  const { name, email, role } = userDetails;

  const subject = [
    { value: "Mathematics", label: "Mathematics" },
    { value: "English", label: "English" },
    { value: "Literature-in-English", label: "Literature-in-English" },
  ];

  const classes = [
    { value: "Primary 1", label: "Primary 1" },
    { value: "Primary 2", label: "Primary 2" },
    { value: "Primary 3", label: "Primary 3" },
  ];

  console.log(profilePicture);

  const handleSubjectChange = (selectedOption) => {
    setSubjectOption(selectedOption);
  };

  const handleClassChange = (selectedOption) => {
    setClassOption(selectedOption);
  };

  const handleProfilePicture = (e) => {
    setProfilePicture(e.target.files[0]);
  };

  const handleChange = (e) => {
    const { value, name } = e.target;

    setUserDetails({ [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);

    const fd = new FormData();
    fd.append("image", profilePicture);
    localStorage.setItem("profilePicture", profilePicture);

    if (!profilePicture) {
      setLoading(false);
      setModalContent("Please upload a profile picture");
      setSuccessModal(true);
      return;
    } else {
      patchData(baseUrl + apiEndpoints.PROFILE_PICTURE, fd)
        .then((res) => {
          if (res.status === 200) {
            setLoading(false);
            setSuccessModal(true);
            setModalContent("Profile Updated Successfully!");
            console.log(res.data.data);
            setUserDetails({
              name: e.target.name.value,
              email: e.target.email.value,
              role: res.data.data.role,
            });
          }
        })
        .catch((err) => {
          setLoading(false);
          setSuccessModal(true);
          setModalContent("Profile Update Failed! Please try again");
          console.log(err);
        });
    }
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      setSuccessModal(false);
    }, 3000);
    return () => clearTimeout(timer);
  }, [successModal]);

  return (
    <div className="settings">
      <div className="classes_header">
        <h2>Settings</h2>
      </div>
      {successModal ? (
        <div className="settings-modal">{modalContent}</div>
      ) : null}
      <div className="settings-header">
        <div className="settings-image">
          {profilePicture ? (
            <img src={URL.createObjectURL(profilePicture)} alt="Person" />
          ) : (
            <img src={imageUrl} alt="Person" />
          )}
        </div>
        <div className="settings-header-text">
          <h5>{name}</h5>
          <p>{role}</p>
          <label htmlFor="file" className="upload-description">
            {profilePicture ? <p>Change Picture</p> : <p>Upload Picture</p>}
            <BsUpload />
          </label>
          <input
            type="file"
            accept=".png, .jpg, .jpeg"
            id="file"
            style={{ display: "none" }}
            onChange={handleProfilePicture}
          />
        </div>
      </div>

      <p className="settings-edit">Edit your details</p>

      <DashboardCard>
        <form onSubmit={handleSubmit}>
          <div className="settings-content">
            <div className="settings-content-items">
              <h5>Tutor Name</h5>
              <input
                type="text"
                name="name"
                value={name}
                onChange={handleChange}
                placeholder="Aanuoluwapo Feyisetan"
                className="settings-description"
              />
            </div>
            <div className="settings-content-items">
              <h5>Email Address</h5>
              <input
                type="email"
                name="email"
                value={email}
                onChange={handleChange}
                placeholder="aanu@intelligentinnovations.io"
                className="settings-description"
              />
            </div>
            <div className="settings-content-items">
              <h5>Assigned Subject</h5>
              <Select
                options={subject}
                defaultValue={subject[0]}
                onChange={handleSubjectChange}
                menuPlacement="top"
                className="react-select-container"
                classNamePrefix="react-select"
              />
            </div>
            <div className="settings-content-items">
              <h5>Assigned Class</h5>
              <Select
                options={classes}
                defaultValue={classes[0]}
                onChange={handleClassChange}
                menuPlacement="top"
                className="react-select-container"
                classNamePrefix="react-select"
              />
            </div>

            <Button disabled={loading}>
              {loading ? (
                <PulseLoader size="10px" color="#b4c304" />
              ) : (
                "Save Details"
              )}
            </Button>
          </div>
        </form>
      </DashboardCard>
    </div>
  );
};

export default Settings;
