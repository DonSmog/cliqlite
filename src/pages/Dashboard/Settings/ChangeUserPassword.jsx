/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import "../../../styles/change-user-password.css";
import { FiChevronLeft, FiEye, FiEyeOff } from "react-icons/fi";
import { useNavigate } from "react-router-dom";
import Button from "../../../components/Button";
import DashboardCard from "../../../components/DashboardCard";
import ROUTES from "../../../Helpers/routes";

const ChangeUserPassword = () => {
  const navigate = useNavigate();

  const [userCredentials, setUserCredentials] = useState({
    oldPassword: "",
    password: "",
    confirmPassword: "",
  });
  const { oldPassword, password, confirmPassword } = userCredentials;

  const [modal, setModal] = useState(false);
  const [modalContent, setModalContent] = useState(null);
  const [showOldPassword, setShowOldPassword] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();

    // if (password === "" || confirmPassword === "") {
    //   setModal(true);
    //   setModalContent("Password and Confirm Password is required");
    // } else if (password !== confirmPassword) {
    //   setModal(true);
    //   setModalContent("Password and Confirm Password does not match");
    // } else if (password.length < 6) {
    //   setModal(true);
    //   setModalContent("Password must be at least 6 characters");
    // } else {
    //   navigate(ROUTES.PASSWORD_CHANGE_SUCCESS);
    // }
  };

  const handleChange = (event) => {
    const { value, name } = event.target;

    setUserCredentials({ ...userCredentials, [name]: value });
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      setModal(false);
    }, 3000);
    return () => clearTimeout(timer);
  }, [modal]);

  const handleClickShowOldPassword = () => {
    setShowOldPassword(!showOldPassword);
  };

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleClickConfirmPassword = () => {
    setShowConfirmPassword(!showConfirmPassword);
  };

  return (
    <div className="change-user-passwords">
      <div className="view-courses_header">
        <div className="view-courses_header_arrow">
          <div className="arrow-icon" onClick={() => navigate(ROUTES.SETTINGS)}>
            <FiChevronLeft />
            <span>Back</span>
          </div>
        </div>
      </div>

      <DashboardCard>
        <div className="change-user-password">
          <h2>Change Password</h2>
          <p>Please enter your new password and confirm new password</p>

          <form className="form" onSubmit={handleSubmit}>
            {modal ? <div className="long-modal">{modalContent}</div> : null}
            <div className="form-group">
              <input
                name="oldPassword"
                type={showOldPassword ? "text" : "password"}
                placeholder="Enter old Password"
                value={oldPassword}
                onChange={handleChange}
              />
            </div>
            <div className="eyes1" onClick={handleClickShowOldPassword}>
              {showOldPassword ? <FiEyeOff /> : <FiEye />}
            </div>

            <div className="form-group">
              <input
                name="password"
                type={showPassword ? "text" : "password"}
                placeholder="Enter new Password"
                value={password}
                onChange={handleChange}
              />
            </div>
            <div className="eyes2" onClick={handleClickShowPassword}>
              {showPassword ? <FiEyeOff /> : <FiEye />}
            </div>
            <div className="form-group">
              <input
                name="confirmPassword"
                type={showConfirmPassword ? "text" : "password"}
                placeholder="Confirm Password"
                value={confirmPassword}
                onChange={handleChange}
              />
            </div>
            <div className="eyes3" onClick={handleClickConfirmPassword}>
              {showConfirmPassword ? <FiEyeOff /> : <FiEye />}
            </div>

            <Button>Continue</Button>
          </form>
        </div>
      </DashboardCard>
    </div>
  );
};

export default ChangeUserPassword;
