import React from "react";
import DashboardCard from "../../components/DashboardCard";
import "../../styles/feedback.css";

const Feedback = () => {
  return (
    <div className="feedback">
      <div className="classes_header">
        <h2>Feedback</h2>
      </div>

      <div className="feedback-boxes">
        <div className="feedback-boxes-box">
          <h5>Total Feedback</h5>
          <p>350</p>
        </div>
        <div className="feedback-boxes-box">
          <h5>Positive Feedback</h5>
          <p>86%</p>
        </div>
        <div className="feedback-boxes-box">
          <h5>Negative Feedback</h5>
          <p>14%</p>
        </div>
      </div>

      <DashboardCard>Content</DashboardCard>
    </div>
  );
};

export default Feedback;
