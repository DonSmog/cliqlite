import React from "react";
import "../../../styles/quiz.css";

import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import QuizCard from "../../../components/QuizCard";

const Quiz = ({ option1, option2 }) => {
  return (
    <div className="quiz">
      <div className="quiz_header">
        <h2>Quiz</h2>
      </div>

      <Tabs>
        <TabList className="dropdown_tablist">
          <Tab>All (20)</Tab>
        </TabList>

        <TabPanel>
          <QuizCard
            option1="View Quiz"
            option2="Add Questions"
            option3="Delete Quiz"
          />
        </TabPanel>
      </Tabs>
    </div>
  );
};

export default Quiz;
