/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import "../../../styles/addTopics.css";

import DashboardCard from "../../../components/DashboardCard";
import Select from "react-select";
import { postData, baseUrl } from "../../../apis/apiMethods";
import { BsUpload } from "react-icons/bs";
import Button from "../../../components/Button";
import PulseLoader from "react-spinners/PulseLoader";
import ROUTES from "../../../Helpers/routes";
import { useNavigate } from "react-router-dom";

const AddQuizCard = ({ header }) => {
  const navigate = useNavigate();
  const className = localStorage.getItem("className");
  const subject = localStorage.getItem("subject");
  const topicTitle = localStorage.getItem("topic");

  const [selectedOption, setSelectedOption] = useState(null);
  const [categoryOption, setCategoryOption] = useState(null);
  const [quizImage, setQuizImage] = useState(null);
  const [loading, setLoading] = useState(false);
  const [successModal, setSuccessModal] = useState(false);
  const [modalContent, setModalContent] = useState(null);
  const [quizTitle, setQuizTitle] = useState({
    title: topicTitle,
    topic: topicTitle,
  });

  const { title, topic } = quizTitle;

  const subjectOptions = [{ value: subject, label: subject }];

  const categoryOptions = [{ value: className, label: className }];

  const handleCategoryChange = (e) => {
    setCategoryOption(e);
  };

  const handleSubjectChange = (e) => {
    setSelectedOption(e);
  };

  const onChange = (e) => {
    const { value, name } = e.target;
    setQuizTitle({ ...quizTitle, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    const formData = new FormData();
    formData.append("title", title);
    formData.append("image", quizImage);
    formData.append("topic", topic);

    const topicId = localStorage.getItem("topicId");

    if (!title) {
      setLoading(false);
      setSuccessModal(true);
      setModalContent("Please enter a title");
    } else {
      postData(baseUrl + `quizzes/${topicId}`, formData)
        .then((res) => {
          setLoading(false);
          setModalContent("Quiz added successfully");
          setSuccessModal(true);
          localStorage.setItem("quizId", res.data.data._id);

          const timer = setTimeout(() => {
            navigate(ROUTES.ADD_QUESTIONS);
          }, 3000);
          return () => clearTimeout(timer);
        })
        .catch((err) => {
          setLoading(false);
          setModalContent("Error adding quiz. Please try again");
          setSuccessModal(true);
        });
    }
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      setSuccessModal(false);
    }, 3000);
    return () => clearTimeout(timer);
  }, [successModal]);

  return (
    <div className="view-courses">
      {successModal ? (
        <div className="settings-modal">{modalContent}</div>
      ) : null}
      <DashboardCard>
        <div className="add-topic_header">
          <div className="add-topic_header-title">{header}</div>
          <form className="add-topic_header-body" onSubmit={handleSubmit}>
            {quizImage ? (
              <div className="add-topic-image">
                <label>
                  <input type="checkbox" />
                  <img
                    src={URL.createObjectURL(quizImage)}
                    alt="Topic"
                    title="Topic"
                  />
                </label>
                <label
                  htmlFor="quizImage"
                  onClick={(e) => setQuizImage(e.target.files[0])}
                >
                  <p>Change Cover Art</p>
                </label>
              </div>
            ) : (
              <label htmlFor="quizImage" id="pictures">
                <div className="add-topic_header-body_image-cover">
                  <BsUpload />
                  <p>Upload cover art</p>
                </div>
              </label>
            )}

            <input
              type="file"
              accept=".png, .jpg, .jpeg"
              id="quizImage"
              style={{ display: "none" }}
              onChange={(e) => setQuizImage(e.target.files[0])}
            />
            <input
              type="file"
              accept=".png, .jpg, .jpeg"
              id="quizImage"
              style={{ display: "none" }}
            />

            <div className="quiz-questions">
              <div>
                <h5>Quiz Title</h5>
                <input
                  type="text"
                  value={title}
                  name="title"
                  onChange={onChange}
                  placeholder="Enter Title"
                  className="quiz-description"
                />
              </div>
              <div>
                <h5>Subject</h5>
                <Select
                  options={subjectOptions}
                  defaultValue={subjectOptions[0]}
                  placeholder="Select Subject"
                  onChange={handleSubjectChange}
                  menuPlacement="top"
                  className="react-select-container"
                  classNamePrefix="react-select"
                />
              </div>
              <div>
                <h5>Topic</h5>
                <input
                  type="text"
                  value={topic}
                  name="topic"
                  onChange={onChange}
                  placeholder="Enter Topic"
                  className="quiz-description"
                />
              </div>
              <div>
                <h5>Category</h5>
                <Select
                  options={categoryOptions}
                  placeholder="Select Category"
                  defaultValue={categoryOptions[0]}
                  onChange={handleCategoryChange}
                  menuPlacement="top"
                  className="react-select-container"
                  classNamePrefix="react-select"
                />
              </div>
            </div>
            <Button disabled={loading}>
              {loading ? <PulseLoader size="10px" color="#b4c304" /> : "Save"}
            </Button>
          </form>
        </div>
      </DashboardCard>
    </div>
  );
};

export default AddQuizCard;
