import React from "react";
import { FiChevronLeft } from "react-icons/fi";
import Questions from "../../../components/Questions";
import { useNavigate } from "react-router-dom";
import ROUTES from "../../../Helpers/routes";

const AddQuizQuestions = () => {
  const navigate = useNavigate();

  const quizTitle = localStorage.getItem("topic");
  return (
    <div>
      <div className="view-courses_header">
        <div className="view-courses_header_arrow">
          <div className="arrow-icon" onClick={() => navigate(ROUTES.QUIZ)}>
            <FiChevronLeft />
            <span>Back</span>
          </div>
        </div>
      </div>
      <div
        style={{
          fontFamily: "var(--font-family)",
          fontWeight: "bold",
          fontSize: "21px",
          lineHeight: "36px",
          color: "#016f54",
          marginTop: "0.5rem",
        }}
      >
        Add Questions for {quizTitle} Quiz
      </div>
      <Questions />
    </div>
  );
};

export default AddQuizQuestions;
