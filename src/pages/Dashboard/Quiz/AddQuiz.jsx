/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import "../../../styles/addTopics.css";
import { useNavigate } from "react-router-dom";

import { FiChevronLeft } from "react-icons/fi";
import Questions from "../../../components/Questions";
import ROUTES from "../../../Helpers/routes";
import AddQuizCard from "./AddQuizCard";

const AddQuiz = () => {
  const navigate = useNavigate();

  return (
    <div className="view-courses">
      <div className="view-courses_header">
        <div className="view-courses_header_arrow">
          <div className="arrow-icon" onClick={() => navigate(ROUTES.QUIZ)}>
            <FiChevronLeft />
            <span>Back</span>
          </div>
        </div>
      </div>
      <div
        style={{
          fontFamily: "var(--font-family)",
          fontWeight: "bold",
          fontSize: "21px",
          lineHeight: "36px",
          color: "#016f54",
          marginTop: "0.5rem",
        }}
      >
        New Quiz
      </div>

      <AddQuizCard />
    </div>
  );
};

export default AddQuiz;
