/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import "../../../styles/classes.css";
import { useNavigate } from "react-router-dom";

import Pagination from "react-bootstrap-4-pagination";
import Select from "react-select";
import ROUTES from "../../../Helpers/routes";
import DashboardCard from "../../../components/DashboardCard";
import { FaSearch } from "react-icons/fa";
import { baseUrl } from "../../../apis/apiMethods";
import { getData } from "../../../apis/apiMethods";
import { apiEndpoints } from "../../../apis/apiEndpoints";
import MoonLoader from "react-spinners/MoonLoader";

const Classes = () => {
  const [searchTerm, setSearchTerm] = useState("");
  const [classInfo, setClassInfo] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [firstItem, setFirstItem] = useState(0);
  const [itemsPerPage, setItemsPerPage] = useState(5);
  const [showItems, setShowItems] = useState(5);
  const [selectedOption, setSelectedOption] = useState(null);
  const [totalPages, setTotalPages] = useState(1);
  const [loading, setLoading] = useState(false);

  const options = [
    { value: "5", label: "5" },
    { value: "10", label: "10" },
    { value: "15", label: "15" },
    { value: "20", label: "20" },
  ];

  const navigate = useNavigate();

  const handleOnChange = (e) => {
    setSearchTerm(e.target.value);
  };

  const handleOnSubmit = (e) => {
    e.preventDefault();

    if (searchTerm) {
      setSearchTerm("");
    }
  };

  const handleView = (_id, name) => {
    localStorage.setItem("classId", _id);
    localStorage.setItem("className", name);
    navigate(ROUTES.VIEW_COURSES);
  };

  const handleChange = (selectedOption) => {
    setSelectedOption(selectedOption);
    setItemsPerPage(selectedOption.value);
    setShowItems(selectedOption.value);
  };

  const handlePages = (pageNumber) => {
    console.log(pageNumber);
    // const totalPages={Math.ceil(data.length / showItems)}
    if (pageNumber > 1) {
      setFirstItem(firstItem + 5);
      setItemsPerPage(itemsPerPage + 5);
      setCurrentPage(pageNumber);
    }
    // else if (pageNumber < 3) {
    //   setFirstItem(firstItem - 5);
    //   setItemsPerPage(itemsPerPage - 5);
    //   setCurrentPage(pageNumber);
    // }
  };

  const fetchData = () => {
    setLoading(true);

    getData(baseUrl + apiEndpoints.GRADES).then((res) => {
      setLoading(false);
      setClassInfo(res.data.data);
      setTotalPages(Math.ceil(res.data.data.length / showItems));
    });
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="classes">
      <div className="classes_header">
        <h2>Assigned Classes</h2>
      </div>

      <DashboardCard>
        <div className="classes_content">
          <div className="classes_content_top">
            <form onSubmit={handleOnSubmit} className="classes_search">
              <div className="icon">
                <FaSearch />
              </div>
              <input
                onChange={handleOnChange}
                type="text"
                value={searchTerm}
                placeholder="Search Class"
              />
            </form>

            <div className="class-tables">
              <table className="table-header">
                <thead>
                  <tr>
                    <th>Class</th>
                    <th>Number of Subjects</th>
                    <th>Number of Topics</th>
                    <th>Action</th>
                  </tr>
                </thead>
              </table>
              {loading ? (
                <div className="loading">
                  <MoonLoader color="var(--button)" />
                </div>
              ) : (
                <table className="table-content">
                  {classInfo.map((item) => (
                    <tbody key={item._id}>
                      <tr>
                        <th>{item.name}</th>
                        <td>{item.subjectsNo}</td>
                        <td>{item.topicsNo}</td>
                        <td>
                          <div className="table-links">
                            <label
                              onClick={() => handleView(item._id, item.name)}
                            >
                              View
                            </label>
                            <label>Edit</label>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  ))}
                </table>
              )}
            </div>
          </div>
          <div className="class-line">
            <span></span>
          </div>
        </div>
        <div className="classes-footer">
          <div className="selections">
            <Select
              options={options}
              defaultValue={options[0]}
              onChange={handleChange}
              menuPlacement="top"
              className="react-select-classes-container"
            />
            <h5>
              Showing {firstItem + 1} - {itemsPerPage} of{" "}
              {Math.ceil(classInfo.length / showItems)}
            </h5>
          </div>
          <div className="pagination">
            <Pagination
              threeDots={true}
              totalPages={totalPages}
              currentPage={currentPage}
              onClick={handlePages}
              showMax={5}
              prevNext
              size="sm"
              color="#618B05"
              activeBgColor="#016F54"
              activeBorderColor="#016F54"
              activeColor="#fff"
              textColor="#618B05"
            />
          </div>
        </div>
      </DashboardCard>
    </div>
  );
};

export default Classes;
