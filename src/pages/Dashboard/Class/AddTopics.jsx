/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import "../../../styles/addTopics.css";
import { useNavigate } from "react-router-dom";

import DashboardCard from "../../../components/DashboardCard";

import { FiChevronLeft } from "react-icons/fi";
import Button from "../../../components/Button";
import ROUTES from "../../../Helpers/routes";
import PulseLoader from "react-spinners/PulseLoader";
import { postData, baseUrl } from "../../../apis/apiMethods";
import { apiEndpoints } from "../../../apis/apiEndpoints";

const AddTopics = () => {
  const navigate = useNavigate();
  const [addTopicLoading, setAddTopicLoading] = useState(false);
  const [successModal, setSuccessModal] = useState(false);
  const [modalContent, setModalContent] = useState("");
  const [topicTitle, setTopicTitle] = useState({
    title: "",
    description: "",
  });

  const { title, description } = topicTitle;

  const handleTitleChange = (e) => {
    const { name, value } = e.target;
    setTopicTitle({ ...topicTitle, [name]: value });
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      setSuccessModal(false);
    }, 3000);
    return () => clearTimeout(timer);
  }, [successModal]);

  const handleAddTopic = (e) => {
    e.preventDefault();

    const subjectId = localStorage.getItem("subjectId");

    setAddTopicLoading(true);
    const data = {
      name: title,
      description: description,
      subject: subjectId,
    };

    if (!title) {
      setAddTopicLoading(false);
      setSuccessModal(true);
      setModalContent("Please Add Topic Title");
    } else {
      postData(baseUrl + apiEndpoints.ADD_TOPIC, data)
        .then((res) => {
          if (res.status === 201) {
            setAddTopicLoading(false);
            setModalContent("Topic sent for Approval");
            localStorage.setItem("topicId", res.data.data._id);
            setSuccessModal(true);
            setTopicTitle({
              title: "",
              description: "",
            });

            const timer = setTimeout(() => {
              navigate(ROUTES.VIEW_COURSES);
            }, 300);
            return () => clearTimeout(timer);
          }
        })
        .catch((err) => {
          setAddTopicLoading(false);
          setModalContent("Error adding Topic. Please try again");
          setSuccessModal(true);
          setTopicTitle({
            title: "",
            description: "",
          });

          setTimeout(() => {
            navigate(ROUTES.VIEW_COURSES);
          }, 3500);
        });
    }
  };

  return (
    <div className="view-courses">
      <div className="view-courses_header">
        {successModal ? (
          <div className="settings-modal">{modalContent}</div>
        ) : null}
        <div className="view-courses_header_arrow">
          <div
            className="arrow-icon"
            onClick={() => navigate(ROUTES.VIEW_COURSES)}
          >
            <FiChevronLeft />
            <span>Back</span>
          </div>
        </div>
      </div>

      <DashboardCard>
        <div className="add-topic_header">
          <div className="add-topic_header-title">Add Topic</div>
          <form className="add-topic_header-body" onSubmit={handleAddTopic}>
            <h5>Topic Title</h5>
            <input
              type="text"
              onChange={handleTitleChange}
              value={title}
              name="title"
              id="topicTitle"
              placeholder="Topic Title"
              className="topic-description"
            />
            <h5>Topic Description</h5>
            <textarea
              type="text"
              placeholder="Topic Description"
              className="video-description"
              onChange={handleTitleChange}
              value={description}
              name="description"
            />

            <Button disabled={addTopicLoading}>
              {addTopicLoading ? (
                <PulseLoader size="10px" color="#b4c304" />
              ) : (
                "Save"
              )}
            </Button>
          </form>
        </div>
      </DashboardCard>
    </div>
  );
};

export default AddTopics;
