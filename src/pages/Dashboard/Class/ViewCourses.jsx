/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import "../../../styles/view-courses.css";

import DashboardCard from "../../../components/DashboardCard";
import CoursesDropdown from "../../../components/CoursesDropdown";
import { useNavigate } from "react-router-dom";
import ROUTES from "../../../Helpers/routes";

import data from "../../../Helpers/data";
import Pagination from "react-bootstrap-4-pagination";
import { FiChevronLeft } from "react-icons/fi";
import { RiCheckboxBlankLine, RiDeleteBinLine } from "react-icons/ri";
import { BsPencil } from "react-icons/bs";
import { CgMathPlus } from "react-icons/cg";
import { baseUrl } from "../../../apis/apiMethods";
import { getData } from "../../../apis/apiMethods";
import { apiEndpoints } from "../../../apis/apiEndpoints";
import MoonLoader from "react-spinners/MoonLoader";
import { v4 as uuid } from "uuid";
import { useAuth } from "../../../context/auth.context";

import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel,
} from "react-accessible-accordion";

const ViewCourses = () => {
  const navigate = useNavigate();
  const { setSubjectID, setTopicID } = useAuth();

  const [classInfo, setClassInfo] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [itemsPerPage] = useState(5);
  const [loading, setLoading] = useState(false);

  const handlePages = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const handleAddTopic = (subjectId) => {
    navigate(ROUTES.ADD_TOPIC);
    localStorage.setItem("subjectId", subjectId);
  };

  const fetchData = () => {
    setLoading(true);

    const classIdNo = localStorage.getItem("classId");

    getData(baseUrl + apiEndpoints.TOPICS_BY_GRADE + classIdNo).then((res) => {
      setLoading(false);
      setClassInfo(res.data.data);
    });
  };

  useEffect(() => {
    fetchData();
  }, []);

  const className = localStorage.getItem("className");

  const handleSelectedTopic = (_id, subjectId, subject, name) => {
    localStorage.setItem("subjectId", subjectId);
    localStorage.setItem("topicId", _id);
    localStorage.setItem("topic", name);
    localStorage.setItem("subject", subject);

    setTopicID(_id);
    setSubjectID(subjectId);
  };

  return (
    <div className="view-courses">
      <div className="view-courses_header">
        <div className="view-courses_header_arrow">
          <div
            className="arrow-icon"
            onClick={() => navigate(ROUTES.DASHBOARD)}
          >
            <FiChevronLeft />
            <span>Back</span>
          </div>
        </div>
        <h2>{className}</h2>
      </div>
      <DashboardCard>
        {loading ? (
          <div className="loading">
            <MoonLoader color="var(--button)" />
          </div>
        ) : (
          <div className="view-courses_card" key={uuid()}>
            {classInfo?.map((item) => {
              const { data, data1, data2 } = item;
              localStorage.setItem("subjectId", data.subjectId);

              return (
                <div key={uuid()} className="view-courses_card_topics">
                  <div className="view-courses_card_topics-title">
                    <div className="view-courses_card_topics-title_left">
                      <RiCheckboxBlankLine />
                      <h4>
                        {data.subject} ({data.topics.length} Topics)
                      </h4>
                    </div>
                    <div className="view-courses_card_topics-title_right">
                      <div
                        className="addition-button"
                        onClick={() => handleAddTopic(data.subjectId)}
                      >
                        <CgMathPlus />
                      </div>
                      <BsPencil />
                      <RiDeleteBinLine />
                    </div>
                  </div>
                  <div className="view-courses_card_topics-topics">
                    <Accordion allowZeroExpanded>
                      {data.topics.map((topic) => (
                        <div
                          className="view-courses_card_topics-topics_list"
                          key={uuid()}
                        >
                          <AccordionItem>
                            <AccordionItemHeading>
                              <AccordionItemButton>
                                <div
                                  key={uuid()}
                                  onClick={() =>
                                    handleSelectedTopic(
                                      topic._id,
                                      data.subjectId,
                                      data.subject,
                                      topic.name
                                    )
                                  }
                                  className="view-courses_card_topics-topics_header"
                                >
                                  <ol>{topic.name}</ol>
                                  <BsPencil />
                                </div>
                              </AccordionItemButton>
                            </AccordionItemHeading>
                            <AccordionItemPanel>
                              <CoursesDropdown topicId={topic._id} />
                            </AccordionItemPanel>
                          </AccordionItem>
                        </div>
                      ))}
                    </Accordion>
                  </div>

                  <div className="view-courses_card_topics-title">
                    <div className="view-courses_card_topics-title_left">
                      <RiCheckboxBlankLine />
                      <h4>
                        {data1.subject} ({data1.topics.length} Topics)
                      </h4>
                    </div>
                    <div className="view-courses_card_topics-title_right">
                      <div
                        className="addition-button"
                        onClick={() => handleAddTopic(data1.subjectId)}
                      >
                        <CgMathPlus />
                      </div>
                      <BsPencil />
                      <RiDeleteBinLine />
                    </div>
                  </div>
                  <div className="view-courses_card_topics-topics">
                    <Accordion allowZeroExpanded>
                      {data1.topics.map((topic) => (
                        <div
                          className="view-courses_card_topics-topics_list"
                          key={uuid()}
                        >
                          <AccordionItem>
                            <AccordionItemHeading>
                              <AccordionItemButton>
                                <div
                                  className="view-courses_card_topics-topics_header"
                                  key={uuid()}
                                  onClick={() =>
                                    handleSelectedTopic(
                                      topic._id,
                                      data1.subjectId,
                                      data1.subject,
                                      topic.name
                                    )
                                  }
                                >
                                  <ol>{topic.name}</ol>
                                  <BsPencil />
                                </div>
                              </AccordionItemButton>
                            </AccordionItemHeading>
                            <AccordionItemPanel>
                              <CoursesDropdown topicId={topic._id} />
                            </AccordionItemPanel>
                          </AccordionItem>
                        </div>
                      ))}
                    </Accordion>
                  </div>

                  <div className="view-courses_card_topics-title">
                    <div className="view-courses_card_topics-title_left">
                      <RiCheckboxBlankLine />
                      <h4>
                        {data2.subject} ({data2.topics.length} Topics)
                      </h4>
                    </div>
                    <div className="view-courses_card_topics-title_right">
                      <div
                        className="addition-button"
                        onClick={() => handleAddTopic(data2.subjectId)}
                      >
                        <CgMathPlus />
                      </div>
                      <BsPencil />
                      <RiDeleteBinLine />
                    </div>
                  </div>
                  <div className="view-courses_card_topics-topics">
                    <Accordion allowZeroExpanded>
                      {data2.topics.map((topic) => (
                        <div
                          className="view-courses_card_topics-topics_list"
                          key={uuid()}
                        >
                          <AccordionItem>
                            <AccordionItemHeading>
                              <AccordionItemButton>
                                <div
                                  className="view-courses_card_topics-topics_header"
                                  key={uuid()}
                                  onClick={() =>
                                    handleSelectedTopic(
                                      topic._id,
                                      data2.subjectId,
                                      data2.subject,
                                      topic.name
                                    )
                                  }
                                >
                                  <ol>{topic.name}</ol>
                                  <BsPencil />
                                </div>
                              </AccordionItemButton>
                            </AccordionItemHeading>
                            <AccordionItemPanel>
                              <CoursesDropdown topicId={topic._id} />
                            </AccordionItemPanel>
                          </AccordionItem>
                        </div>
                      ))}
                    </Accordion>
                  </div>
                </div>
              );
            })}

            <div className="view-courses_pagination">
              <Pagination
                threeDots={true}
                totalPages={Math.ceil(data.length / itemsPerPage)}
                currentPage={currentPage}
                onClick={handlePages}
                showMax={5}
                prevNext
                size="sm"
                color="#618B05"
                activeBgColor="#016F54"
                activeBorderColor="#016F54"
                activeColor="#fff"
                textColor="#618B05"
              />
            </div>
          </div>
        )}
      </DashboardCard>
    </div>
  );
};

export default ViewCourses;
