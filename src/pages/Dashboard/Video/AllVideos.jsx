import React from "react";
import "../../../styles/all-videos.css";
import { useNavigate } from "react-router-dom";
import Button from "../../../components/Button";

import VideoCard from "../../../components/VideoCard";
import ROUTES from "../../../Helpers/routes";

const AllVideos = () => {
  const navigate = useNavigate();

  const handleSubmit = () => {
    navigate(ROUTES.ADD_VIDEO);
  };

  return (
    <div className="all-videos">
      <div className="video_header">
        <h2>Video</h2>
      </div>
      <div className="video-header_top">
        <p>All Videos</p>
        <Button handleSubmit={handleSubmit}>Upload Video</Button>
      </div>
      <VideoCard option1={"View Video"} option2={"Delete Video"} />
    </div>
  );
};

export default AllVideos;
