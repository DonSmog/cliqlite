/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import "../../../styles/add-video.css";

import { BsCameraVideo, BsUpload } from "react-icons/bs";
import Button from "../../../components/Button";
import DashboardCard from "../../../components/DashboardCard";
import Select from "react-select";
import { postData, baseUrl } from "../../../apis/apiMethods";
import PulseLoader from "react-spinners/PulseLoader";
import { useNavigate } from "react-router-dom";
import ROUTES from "../../../Helpers/routes";

const AddVideoCard = ({ header }) => {
  const navigate = useNavigate();
  const className = localStorage.getItem("className");
  const subject = localStorage.getItem("subject");
  const topicTitle = localStorage.getItem("topic");

  const [subjectOption, setSubjectOption] = useState(null);
  const [categoryOption, setCategoryOption] = useState(null);
  const [source, setSource] = useState("");
  const [video, setVideo] = useState("");
  const [loading, setLoading] = useState(false);
  const [successModal, setSuccessModal] = useState(false);
  const [modalContent, setModalContent] = useState(null);
  const [videoTitle, setVideoTitle] = useState({
    title: topicTitle,
    description: "",
  });

  const { title, description } = videoTitle;

  const subjectOptions = [{ value: subject, label: subject }];

  const categoryOptions = [{ value: className, label: className }];

  const handleSubjectChange = (selectedOption) => {
    setSubjectOption(selectedOption);
  };

  const handleCategoryChange = (selectedOption) => {
    setCategoryOption(selectedOption);
  };

  const handleFileChange = (e) => {
    const file = e.target.files[0];
    const url = URL.createObjectURL(file);
    setSource(url);
  };

  const handleVideoChange = (e) => {
    const video = e.target.files[0];
    setVideo(video);
  };

  const onChange = (e) => {
    const { value, name } = e.target;
    setVideoTitle({ ...videoTitle, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    setLoading(true);
    const fd = new FormData();
    fd.append("image", source);
    fd.append("video", video);

    const topicId = localStorage.getItem("topicId");

    if (!video) {
      setModalContent("Please select a video");
      setLoading(false);
      setSuccessModal(true);
    } else
      postData(baseUrl + `topics/${topicId}/video`, fd)
        .then((res) => {
          if (res.status === 200) {
            setLoading(false);
            setSuccessModal(true);
            setModalContent("Video Uploaded Successfully!");
            setVideo("");
            setSource("");

            const timer = setTimeout(() => {
              navigate(ROUTES.VIEW_COURSES);
            }, 3000);
            return () => clearTimeout(timer);
          }
        })
        .catch((err) => {
          setLoading(false);
          setSuccessModal(true);
          setModalContent("Video Upload Failed!");
        });
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      setSuccessModal(false);
    }, 3000);
    return () => clearTimeout(timer);
  }, [successModal]);

  return (
    <div>
      <DashboardCard>
        <div className="add-topic_header">
          {successModal ? (
            <div className="settings-modal">{modalContent}</div>
          ) : null}
          <div className="add-topic_header-title">{header}</div>
          <form className="add-topic_header-body" onSubmit={handleSubmit}>
            {source ? (
              <div className="add-topic-image">
                <label>
                  <input type="checkbox" />
                  <img src={source} alt="Topic" title="Topic" />
                </label>
                <label htmlFor="videImage" onClick={handleFileChange}>
                  <p>Change Cover Art</p>
                </label>
              </div>
            ) : (
              <label htmlFor="videImage" id="pictures">
                <div className="add-topic_header-body_image-cover">
                  <BsUpload />
                  <p>Upload cover art</p>
                </div>
              </label>
            )}
            <input
              type="file"
              accept=".png, .jpg, .jpeg"
              id="videImage"
              style={{ display: "none" }}
              onChange={handleFileChange}
            />
            <div className="quiz-questions">
              <div>
                <h5>Video Title</h5>
                <input
                  type="text"
                  name="title"
                  value={title}
                  onChange={onChange}
                  placeholder="Video Title"
                  className="video-title-description"
                />
              </div>
              <div>
                <h5>Subject</h5>
                <Select
                  options={subjectOptions}
                  defaultValue={subjectOptions[0]}
                  placeholder="Select Subject"
                  onChange={handleSubjectChange}
                  menuPlacement="top"
                  className="react-select-container"
                  classNamePrefix="react-select"
                />
              </div>

              <div>
                <h5>Video Description</h5>
                <textarea
                  type="text"
                  name="description"
                  value={description}
                  onChange={onChange}
                  placeholder="Video Description"
                  className="video-title-description"
                />
              </div>
              <div>
                <h5>Category</h5>
                <Select
                  options={categoryOptions}
                  placeholder="Select Category"
                  defaultValue={categoryOptions[0]}
                  onChange={handleCategoryChange}
                  menuPlacement="top"
                  className="react-select-container"
                  classNamePrefix="react-select"
                />
              </div>
            </div>

            {video ? (
              <div className="add-topic-image">
                <label>
                  <video
                    height={400}
                    src={URL.createObjectURL(video)}
                    controls
                    alt="Topic"
                    title="Topic"
                    id="video-upload"
                  />
                </label>
                <label htmlFor="video" onClick={handleVideoChange}>
                  <p>Upload a different Video</p>
                </label>
              </div>
            ) : (
              <label htmlFor="video" id="videos">
                <BsCameraVideo />
                <p>Click to Upload Video</p>
              </label>
            )}
            <input
              type="file"
              accept=".mov, .mp4, .avi"
              id="video"
              style={{ display: "none" }}
              onChange={handleVideoChange}
            />

            <Button disabled={loading}>
              {loading ? <PulseLoader size="10px" color="#b4c304" /> : "Save"}
            </Button>
          </form>
        </div>
      </DashboardCard>
    </div>
  );
};

export default AddVideoCard;
