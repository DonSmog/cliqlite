/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import "../../../styles/video.css";
import { useNavigate } from "react-router-dom";

import Pagination from "react-bootstrap-4-pagination";
import Select from "react-select";

import DashboardCard from "../../../components/DashboardCard";
import ROUTES from "../../../Helpers/routes";
import { FaSearch } from "react-icons/fa";
import { baseUrl } from "../../../apis/apiMethods";
import { getData } from "../../../apis/apiMethods";
import { apiEndpoints } from "../../../apis/apiEndpoints";
import MoonLoader from "react-spinners/MoonLoader";
import { v4 as uuid } from "uuid";

const Video = () => {
  const [searchTerm, setSearchTerm] = useState("");
  const [classInfo, setClassInfo] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [firstItem, setFirstItem] = useState(0);
  const [itemsPerPage, setItemsPerPage] = useState(5);
  const [selectedOption, setSelectedOption] = useState(null);
  const [sortOption, setSortOption] = useState(null);
  const [totalPages, setTotalPages] = useState(1);
  const [loading, setLoading] = useState(false);

  const options = [
    { value: "5", label: "5" },
    { value: "10", label: "10" },
    { value: "15", label: "15" },
    { value: "20", label: "20" },
  ];

  const sort = [
    { value: "Class", label: "Class" },
    { value: "Length", label: "Length" },
    { value: "Topic", label: "Topic" },
  ];

  const navigate = useNavigate();

  const handleOnChange = (e) => {
    setSearchTerm(e.target.value);
  };

  const handleOnSubmit = (e) => {
    e.preventDefault();

    if (searchTerm) {
      setSearchTerm("");
    }
  };

  const handleView = () => {
    navigate(ROUTES.ALL_VIDEO);
  };

  const handleChange = (selectedOption) => {
    setSelectedOption(selectedOption);
    setItemsPerPage(selectedOption.value);
    setFirstItem(0);
  };

  const handleSort = (sortOption) => {
    setSortOption(sortOption);
    // setItemsPerPage(selectedOption.value);
    // setFirstItem(0);
  };

  const handlePages = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const fetchData = () => {
    setLoading(true);

    getData(baseUrl + apiEndpoints.GRADES).then((res) => {
      setLoading(false);
      setClassInfo(res.data.data);
      setTotalPages(Math.ceil(res.data.data.length / itemsPerPage));
    });
  };

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="video">
      <div className="video_header">
        <h2>Video</h2>
      </div>
      <DashboardCard>
        <div className="video_content">
          <div className="video_content_top">
            <div className="video_content_top-bar">
              <form onSubmit={handleOnSubmit} className="video_search">
                <div className="icon">
                  <FaSearch />
                </div>
                <input
                  onChange={handleOnChange}
                  type="text"
                  value={searchTerm}
                  placeholder="Search Class"
                />
              </form>

              <div className="sort-selections">
                <Select
                  options={sort}
                  value={sortOption}
                  placeholder="Sort by"
                  onChange={handleSort}
                  className="react-select-classes-container"
                />
              </div>
            </div>

            <div className="video-tables">
              <table className="video-header">
                <thead>
                  <tr>
                    <th>CLASS</th>
                    <th>STUDENTS</th>
                    <th>SUBJECT</th>
                    <th>TOPICS</th>
                    <th>ACTION</th>
                  </tr>
                </thead>
              </table>
              {loading ? (
                <div className="loading">
                  <MoonLoader color="var(--button)" />
                </div>
              ) : (
                <table className="video-content">
                  {classInfo.map((item) => (
                    <tbody key={uuid()}>
                      <tr>
                        <th>{item.name}</th>
                        <td>10</td>
                        <td>{item.subjectsNo}</td>
                        <td>{item.topicsNo}</td>
                        <td>
                          <div className="video-links">
                            <label onClick={handleView}>View</label>
                            <label>Delete</label>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  ))}
                </table>
              )}
            </div>
          </div>
          <div className="video-line">
            <span></span>
          </div>
        </div>
        <div className="video-footer">
          <div className="selections">
            <Select
              options={options}
              defaultValue={options[0]}
              onChange={handleChange}
              menuPlacement="top"
              className="react-select-classes-container"
            />
            <h5>
              Showing {firstItem + 1} - {itemsPerPage} of {totalPages}
            </h5>
          </div>
          <div className="pagination">
            <Pagination
              threeDots={true}
              totalPages={totalPages}
              currentPage={currentPage}
              onClick={handlePages}
              showMax={5}
              prevNext
              size="sm"
              color="#618B05"
              activeBgColor="#016F54"
              activeBorderColor="#016F54"
              activeColor="#fff"
              textColor="#618B05"
            />
          </div>
        </div>
      </DashboardCard>
    </div>
  );
};

export default Video;
