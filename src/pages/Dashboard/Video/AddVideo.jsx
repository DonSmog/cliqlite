import React from "react";
import "../../../styles/add-video.css";
import { useNavigate } from "react-router-dom";

import { FiChevronLeft } from "react-icons/fi";
import ROUTES from "../../../Helpers/routes";
import AddVideoCard from "./AddVideoCard";

const AddVideo = () => {
  const navigate = useNavigate();

  return (
    <div>
      <div className="view-courses_header">
        <div className="view-courses_header_arrow">
          <div
            className="arrow-icon"
            onClick={() => navigate(ROUTES.ALL_VIDEO)}
          >
            <FiChevronLeft />
            <span>Back</span>
          </div>
        </div>
      </div>
      <div
        style={{
          fontFamily: "var(--font-family)",
          fontWeight: "bold",
          fontSize: "21px",
          lineHeight: "36px",
          color: "#016f54",
          marginTop: "0.5rem",
        }}
      >
        Add Video
      </div>
      <AddVideoCard />
    </div>
  );
};

export default AddVideo;
