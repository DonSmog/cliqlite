import React from "react";
import "../styles/landing-page.css";
import { ImPlay2 } from "react-icons/im";
import { useNavigate } from "react-router-dom";

import Logo from "../assets/Logo.svg";
import children_image from "../assets/children-image.png";
import top_bg from "../assets/top-bg.svg";
import laptop from "../assets/laptop.png";
import laptop_bg from "../assets/laptop_bg.svg";
import video from "../assets/video.png";
import video_bg from "../assets/video-bg.svg";
import analytics from "../assets/analytics.png";
import analytics_bg1 from "../assets/analytics.svg";
import analytics_bg2 from "../assets/analytics1.svg";
import googlePlay from "../assets/googlePlay.png";
import ios from "../assets/ios.png";
import pressing from "../assets/pressing.png";
import pressing_bg from "../assets/pressing_bg.svg";
import fb from "../assets/fb.png";
import instagram from "../assets/instagram.png";
import twitter from "../assets/twitter.png";
import ROUTES from "../Helpers/routes";

const LandingPage = () => {
  const date = new Date().getFullYear();
  const navigate = useNavigate();

  const playGame = () => {
    navigate(ROUTES.PLAYER_INFO);
  };
  return (
    <div className="landing-page">
      <img className="top-bg" src={top_bg} alt="Background" />
      <div className="page-top">
        <div className="logo">
          <img src={Logo} alt="Logo" />
        </div>
        <div className="page-top-body">
          <div className="page-top-body_texts">
            <h1>
              Creativity never Ends, Have <span>fun</span> while{" "}
              <span>learning.</span>
            </h1>
            <h4>
              With our special courses creativity, problem-solving and critical
              thinking skill are nurtured and built up in every child while
              having fun.
            </h4>
          </div>
          <img src={children_image} alt="children" />
        </div>
        <div className="page-top-under">
          <h4>Trusted Kids’ Assessment</h4>
        </div>

        <div className="laptop-section">
          <img className="laptop_bg" src={laptop_bg} alt="Laptop Background" />
          <div className="laptop-section_content">
            <div>
              <h1>
                Get access to over 1000 interactive projects and quizzes with
                solutions.
              </h1>
              <button onClick={playGame}>Play Quiz Games</button>
            </div>
            <img className="laptop-image" src={laptop} alt="Laptop" />
          </div>
        </div>

        <div className="video-section">
          <h2>Learn at your pace</h2>
          <img className="video-bg" src={video_bg} alt="Video BG" />
          <div className="video-section-content">
            <img src={video} alt="Video" />
            <div className="video-section-content_text">
              <h5>
                Every child has the capacity to learn but at a different pace
                and level. With our vast library of video lessons and quiz
                assessments, Every child can learn at their appropriate pace and
                level, ensuring guaranteed academic excellence always.
              </h5>
              <button>
                <ImPlay2 />
                <p>Play Video</p>
              </button>
            </div>
          </div>

          <div className="class-section">
            <div className="class-section_row1">
              <h1>Choose A suitable grade for your child</h1>
              <p>
                Child care is an often overlooked component of child
                development. We promise you that, we will always try to take
                care of your children.
              </p>
            </div>
            <div className="class-section_row2">
              <div className="class-section-row2_grid">
                <span style={{ background: "yellow" }}></span>
                <h5>Grade 1</h5>
                <p>
                  Our contents are tailored to suit the age and grade of each
                  child as our goal is to make learning easy and fun
                </p>
              </div>
              <div className="class-section-row2_grid">
                <span style={{ background: "purple" }}></span>
                <h5>Grade 2</h5>
                <p>
                  Our contents are tailored to suit the age and grade of each
                  child as our goal is to make learning easy and fun
                </p>
              </div>
              <div className="class-section-row2_grid">
                <span style={{ background: "red" }}></span>
                <h5>Grade 3</h5>
                <p>
                  Our contents are tailored to suit the age and grade of each
                  child as our goal is to make learning easy and fun
                </p>
              </div>
              <div className="class-section-row2_grid">
                <span style={{ background: "blue" }}></span>
                <h5>Grade 4</h5>
                <p>
                  Our contents are tailored to suit the age and grade of each
                  child as our goal is to make learning easy and fun
                </p>
              </div>
              <div className="class-section-row2_grid">
                <span style={{ background: "green" }}></span>
                <h5>Grade 5</h5>
                <p>
                  Our contents are tailored to suit the age and grade of each
                  child as our goal is to make learning easy and fun.
                </p>
              </div>
            </div>
          </div>

          <div className="analysis-section">
            <div className="analysis-section_content-row1">
              <h5>Get a Detailed Weekly Progress Report of your child.</h5>
              <p>
                Monitor your child’s learning progress closely with detailed
                weekly reports. Get notified via SMS and email.
              </p>
            </div>
            <div className="analysis-section_content-row2">
              <img className="analytics" src={analytics} alt="analytics" />
              <img
                className="analytics_bg1"
                src={analytics_bg1}
                alt="analytics"
              />
              <img
                className="analytics_bg2"
                src={analytics_bg2}
                alt="analytics"
              />
            </div>
          </div>
        </div>

        <div className="download-section">
          <div className="download-section_row1">
            <h5>
              Get the best online learning platform for your kids now at a
              highly affordable rate,
            </h5>
            <div className="download-icons">
              <img src={googlePlay} alt="Google Play" />
              <img src={ios} alt="iOS" />
            </div>
          </div>
          <div className="download-image">
            <img className="pressing" src={pressing} alt="Pressing Phone" />
            <img className="pressing_bg" src={pressing_bg} alt="Pressing BG" />
          </div>
        </div>
      </div>

      <footer>
        <img src={Logo} alt="Logo" />
        <div className="social-images">
          <img src={fb} alt="Facebook" />
          <img src={twitter} alt="Twitter" />
          <img src={instagram} alt="Instagram" />
        </div>
        <p>© {date} All Rights Reserved</p>
      </footer>
    </div>
  );
};

export default LandingPage;
