import React, { useState, useEffect } from "react";
import "../styles/changepassword.css";
import Card from "../components/Card";
import Button from "../components/Button";
import Background1 from "../assets/Background2.svg";
import Background2 from "../assets/Background1.svg";
import { useNavigate } from "react-router-dom";
import { FiEyeOff, FiEye } from "react-icons/fi";
import ROUTES from "../Helpers/routes";

const ChangePassword = () => {
  const [userCredentials, setUserCredentials] = useState({
    password: "",
    confirmPassword: "",
  });
  const [modal, setModal] = useState(false);
  const [modalContent, setModalContent] = useState(null);
  let navigate = useNavigate();

  const { password, confirmPassword } = userCredentials;
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);

  const handleChange = (event) => {
    const { value, name } = event.target;

    setUserCredentials({ ...userCredentials, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (password === "" || confirmPassword === "") {
      setModal(true);
      setModalContent("Password and Confirm Password is required");
    } else if (password !== confirmPassword) {
      setModal(true);
      setModalContent("Password and Confirm Password does not match");
    } else if (password.length < 6) {
      setModal(true);
      setModalContent("Password must be at least 6 characters");
    } else {
      navigate(ROUTES.PASSWORD_CHANGE_SUCCESS);
    }
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      setModal(false);
    }, 3000);
    return () => clearTimeout(timer);
  }, [modal]);

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleClickConfirmPassword = () => {
    setShowConfirmPassword(!showConfirmPassword);
  };

  return (
    <div className="change-password">
      <Card
        title="Forgot Password"
        text="Please enter your new password and confirm new password"
      >
        <form className="form" onSubmit={handleSubmit}>
          {modal ? <div className="long-modal">{modalContent}</div> : null}
          <div className="form-group">
            <input
              name="password"
              type={showPassword ? "text" : "password"}
              placeholder="Enter new Password"
              value={password}
              onChange={handleChange}
            />
          </div>
          <div className="eyes1" onClick={handleClickShowPassword}>
            {showPassword ? <FiEyeOff /> : <FiEye />}
          </div>
          <div className="form-group">
            <input
              name="confirmPassword"
              type={showConfirmPassword ? "text" : "password"}
              placeholder="Confirm Password"
              value={confirmPassword}
              onChange={handleChange}
            />
          </div>
          <div className="eyes2" onClick={handleClickConfirmPassword}>
            {showConfirmPassword ? <FiEyeOff /> : <FiEye />}
          </div>
        </form>
        <Button handleSubmit={handleSubmit}>Continue</Button>
      </Card>
      <div className="background1">
        <img src={Background1} alt="background" />
      </div>
      <div className="background2">
        <img src={Background2} alt="background" />
      </div>
      <div className="line">
        <span></span>
      </div>
    </div>
  );
};

export default ChangePassword;
