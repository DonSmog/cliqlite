import React, { useState } from "react";
import "../styles/play-quiz.css";
import data from "../Helpers/data";
import ROUTES from "../Helpers/routes";
import { useNavigate } from "react-router-dom";
import { v4 as uuid } from "uuid";
import { MdArrowForwardIos } from "react-icons/md";

import smile from "../assets/smile.png";
import sad from "../assets/sad.png";
import googlePlay from "../assets/googlePlay.png";
import ios from "../assets/ios.png";

const PlayQuizGame = () => {
  const navigate = useNavigate();
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [currentNo, setCurrentNo] = useState(1);
  const [quizData] = useState(data);
  const [points, setPoints] = useState(0);
  const [selectedAnswer, setSelectedAnswer] = useState("");
  const [radioClass, setRadioClass] = useState("radio");
  const [disabled, setDisabled] = useState(false);
  const [modalContent, setModalContent] = useState("");
  const [emoji, setEmoji] = useState();
  const [color, setColor] = useState("");
  const [showScore, setShowScore] = useState(false);
  const [explanation, setExplanation] = useState(false);
  const [correctAnswer, setCorrectAnswer] = useState("");

  const endGame = () => {
    navigate(ROUTES.HOME);
  };

  const isAnswerSelected = (value) => selectedAnswer === value;

  const handleRadioClicked = (e) => {
    setSelectedAnswer(e.target.value);
  };

  const handleAnswer = (isCorrect) => {
    if (isCorrect === true) {
      setRadioClass("radio");
      setDisabled(true);
      setEmoji(smile);
      setExplanation(false);
      setModalContent("Correct! You got 5 points");
      setColor("#006b17");
      setPoints(points + 5);
    } else {
      setRadioClass("radio-wrong");
      setDisabled(true);
      setEmoji(sad);
      setExplanation(true);
      setCorrectAnswer(quizData[currentQuestion].correctAnswer);
      setModalContent(`Oops! Your answer is wrong.`);
      setColor("#EF0000");
    }
  };

  const handleNext = (e) => {
    const nextQuestion = currentQuestion + 1;
    const nextNo = currentNo + 1;
    if (nextQuestion < quizData.length) {
      setCurrentQuestion(nextQuestion);
      setCurrentNo(nextNo);
      setDisabled(false);
      setSelectedAnswer("");
    } else {
      setModalContent(
        "You're killing it already! Download our App to Earn more points"
      );
      setShowScore(true);
      setDisabled(false);
    }
  };

  return (
    <div className="play-quiz">
      {showScore ? (
        <div className="play-quiz-card">
          <div className="play-quiz-card_head">
            <h5>
              Question {currentNo} of {quizData.length}
            </h5>
            <span>Points: {points}</span>
            <button onClick={endGame}>End Game</button>
          </div>
          <div className="play-quiz-card-body">
            <h4>You scored {points} points.</h4>

            {points < 50 ? (
              <h3>You scored so low. Download our App to learn and improve.</h3>
            ) : (
              <h3>{modalContent}</h3>
            )}

            <div className="downloads">
              <img src={googlePlay} alt="google play" />
              <img src={ios} alt="ios" />
            </div>
          </div>
        </div>
      ) : (
        <div className="play-quiz-card">
          <div className="play-quiz-card_head">
            <h5>
              Question {currentNo} of {quizData.length}
            </h5>
            <span>Points: {points}</span>
            <button onClick={endGame}>End Game</button>
          </div>
          <div className="play-quiz-card-body">
            <button>Question {currentNo}</button>
            <h2>{quizData[currentQuestion].questionText}</h2>

            <div className="quiz-answers">
              {quizData[currentQuestion].answerOptions.map((answers) => {
                const { option, answerText, isCorrect } = answers;

                return (
                  <div className="quiz-options" key={uuid()}>
                    <input
                      disabled={disabled}
                      type="radio"
                      className={radioClass}
                      id={option}
                      value={option}
                      checked={isAnswerSelected(option)}
                      onChange={handleRadioClicked}
                      onClick={() => handleAnswer(isCorrect, option)}
                    />
                    <label htmlFor={option}>
                      <p>{option}</p>
                      <h5>{answerText}</h5>
                    </label>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      )}

      {disabled && (
        <div className="quiz-bottom">
          <img src={emoji} alt="Emoji" />
          <h5 style={{ color }}>{modalContent}</h5>
          {explanation && <span>Correct Answer is {correctAnswer}</span>}
          <button onClick={handleNext}>
            Next
            <p>
              <MdArrowForwardIos />
            </p>
          </button>
        </div>
      )}
    </div>
  );
};

export default PlayQuizGame;
