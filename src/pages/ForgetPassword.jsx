import React, { useState, useEffect } from "react";
import "../styles/forget-password.css";
import Card from "../components/Card";
import Button from "../components/Button";
import Background1 from "../assets/Background2.svg";
import Background2 from "../assets/Background1.svg";
import { useNavigate } from "react-router-dom";
import ROUTES from "../Helpers/routes";
import PulseLoader from "react-spinners/PulseLoader";
import { apiEndpoints } from "../apis/apiEndpoints";
import { baseUrl, postData } from "../apis/apiMethods";

const ForgetPassword = () => {
  const [userCredentials, setUserCredentials] = useState({
    email: "",
  });
  const [emailModal, setEmailModal] = useState(false);
  const [modalContent, setModalContent] = useState(null);
  const [loading, setLoading] = useState(false);
  let navigate = useNavigate();

  const { email } = userCredentials;

  const handleChange = (event) => {
    const { value, name } = event.target;

    setUserCredentials({ ...userCredentials, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    setLoading(true);
    const data = {
      email: email,
    };

    if (email === "") {
      setLoading(false);
      setEmailModal(true);
      setModalContent("Please input your email address");
    } else if (!email.includes("@") || !email.includes(".")) {
      setLoading(false);
      setEmailModal(true);
      setModalContent("Please input a valid email address");
    } else {
      postData(baseUrl + apiEndpoints.RECOVER, data)
        .then((res) => {
          if (res.status === 200) {
            setLoading(false);
            localStorage.setItem("email", e.target.email.value);
            navigate(ROUTES.CONFIRM_FORGET_PASSWORD);
          }
        })
        .catch((err) => {
          setLoading(false);
          setModalContent("Sorry, we couldn't find your email address");
          setEmailModal(true);
        });
    }
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      setEmailModal(false);
    }, 3000);
    return () => clearTimeout(timer);
  }, [emailModal]);

  return (
    <div className="forget-password">
      <Card
        title="Forgot Password"
        text="Please enter your registered email address to reset your password"
      >
        <div className="login-form">
          <form onSubmit={handleSubmit}>
            <div className="form-group">
              {emailModal ? (
                <div className="login-modal">{modalContent}</div>
              ) : null}
              <input
                name="email"
                type="email"
                placeholder="Email Address"
                value={email}
                onChange={handleChange}
              />
            </div>
            <Button disabled={loading}>
              {loading ? (
                <PulseLoader size="10px" color="#b4c304" />
              ) : (
                "Continue"
              )}
            </Button>
          </form>
        </div>
      </Card>
      <div className="background1">
        <img src={Background1} alt="background" />
      </div>
      <div className="background2">
        <img src={Background2} alt="background" />
      </div>
      <div className="line">
        <span></span>
      </div>
    </div>
  );
};

export default ForgetPassword;
