import React, { useEffect, useState } from "react";
import "../styles/confirmForgetPassword.css";
import Card from "../components/Card";
import Button from "../components/Button";
import PinInput from "react-pin-input";
import Background1 from "../assets/Background2.svg";
import Background2 from "../assets/Background1.svg";
import { useNavigate } from "react-router-dom";
import PulseLoader from "react-spinners/PulseLoader";
import ROUTES from "../Helpers/routes";
import Countdown from "react-countdown";
import { baseUrl, postData } from "../apis/apiMethods";
import { apiEndpoints } from "../apis/apiEndpoints";

const ConfirmForgetPassword = () => {
  let navigate = useNavigate();

  const [modal, setModal] = useState(false);
  const [modalContent, setModalContent] = useState(null);
  const [loading, setLoading] = useState(false);
  const [pin, setPin] = useState("");
  const pincodeLength = 6;

  const [message] = useState(localStorage.getItem("email"));

  const handleOnChange = (value, idx) => {
    if (value.length < pincodeLength) {
      setPin("");
    }
  };

  const handleResendOTP = () => {
    const getEmail = localStorage.getItem("email");
    const data = {
      email: getEmail,
    };

    postData(baseUrl + apiEndpoints.RESEND_OTP, data).then((res) => {
      if (res.status === 200) {
        console.log(res.data);
        localStorage.setItem("response", res.data.message);
      }
    });
  };

  const handleSubmit = (e) => {
    setLoading(true);
    if (e.length !== pincodeLength) {
      e.preventDefault();
      setLoading(false);
      setModal(true);
      setPin("");
      setModalContent("Please input a valid pin code");
    } else {
      setPin(e);
      navigate(ROUTES.CHANGE_PASSWORD);
    }
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      setModal(false);
    }, 3000);
    return () => clearTimeout(timer);
  }, [modal]);

  const CountDownComplete = () => <p onClick={handleResendOTP}>Resend OTP</p>;

  const renderer = ({ minutes, seconds, completed }) => {
    if (completed) {
      return <CountDownComplete />;
    } else {
      return (
        <span>
          Resend OTP after {minutes}:{seconds} secs
        </span>
      );
    }
  };

  return (
    <div className="confirm-forget-password">
      <Card
        title="Forgot Password"
        text={`A 6-digit code has been sent to ${message}, please enter code below`}
      >
        <div className="login-form">
          <form onSubmit={handleSubmit}>
            <div className="form-group">
              {modal ? <div className="login-modal">{modalContent}</div> : null}
              <PinInput
                length={pincodeLength}
                initialValue={pin}
                secret
                onChange={handleOnChange}
                type="numeric"
                inputMode="number"
                style={{
                  padding: "5px 20px",
                }}
                inputStyle={{
                  borderColor: "#A9A9A9",
                  borderRadius: "10px",
                  fontSize: "1.5rem",
                  width: "40px",
                  margin: "0px 5px",
                }}
                inputFocusStyle={{ borderColor: "#016F54" }}
                onComplete={(value) => handleSubmit(value)}
                autoSelect={true}
                regexCriteria={/^[ A-Za-z0-9_@./#&+-]*$/}
              />
            </div>

            <div className="countdown">
              <Countdown date={Date.now() + 30000} renderer={renderer} />
            </div>

            <Button disabled={loading}>
              {loading ? (
                <PulseLoader size="10px" color="#b4c304" />
              ) : (
                "Continue"
              )}
            </Button>
          </form>
        </div>
      </Card>
      <div className="background1">
        <img src={Background1} alt="background" />
      </div>
      <div className="background2">
        <img src={Background2} alt="background" />
      </div>
      <div className="line">
        <span></span>
      </div>
    </div>
  );
};

export default ConfirmForgetPassword;
